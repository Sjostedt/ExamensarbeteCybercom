%Subscribe and fetch sum data
%Install ROS for matlab
%Install dependencies for custom ROS messages "Robotics System Toolbox
%Interface for ROS Custom Message" 

clc
clear all
%% Define stuff
% Define types
global wheeldiameter
global wheelbase
global encoderpulse
global Ts
global uwb_module_distance

odompos = struct;
encoderpos = struct;
encoderposICC = struct;
encoderpos_old = struct;
encoderposICC_old = struct;
uwb_modulerange = struct;
rotation = struct;

% Define constants
wheeldiameter= 0.24;% Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946;
wheelbase = 0.4645;    % Wheel base [m]
wheelbase = 0.48;    % Wheel base [m]
encoderpulse =  349; %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / pi;
Hz = 50;
Ts = 1/Hz;
uwb_module_distance = 0.2;

%%
%rosinit('10.42.0.1')         % Initiallize ROS
% rosinit()
%Only needs to be done ONCE
% driverpath = fullfile('/','home','micke','ExamensarbeteCybercom','catkin_ws','src', 'hrp'); % Specify path of custom drivers
%rosgenmsg(driverpath); % Load and generate custom msg packages
%addpath('/home/lensund/ExamensarbeteCybercom/catkin_ws/src/hrp/matlab_gen/msggen')
% Follow output of rosgenmsg
%% Subscribe to stuff
EncoderMessage = rossubscriber('/wheel_encoder');
odomdata = rossubscriber('/odom');


pause(2);

%% For Python confirmation
PythonPos = rossubscriber('Encoder_pos');
%% Sample loop
startdisp = sprintf('\n Start of Simulation \n');
disp(startdisp)
run = 1;
for run = 1:15
%Define some variables
% conf95 = 0.25;
% while conf95<0.3
t0 = clock;
n = 1; % Initial value for if
c = 0;

% Initial positions
X_encoder = [];
Y_encoder = [];

X_odom = [];
Y_odom = [];


X_kalman = [];
Y_kalman = [];

X_kalman5 = [];
Y_kalman5 = [];



% pythonposx = [];
% pythonposy = [];

odompos.x = 0;
odompos.y = 0;
odompos.theta = 0;
encoderpos.x = 0;
encoderpos.y = 0;
encoderpos.theta = 0;
encoderposICC.x = 0;
encoderposICC.y = 0;
encoderposICC.theta = 0;
encoderpos_old = encoderpos;
encoderposICC_old = encoderposICC;

distance_old.fr = 0;
distance_old.fl = 0;
distance_old.br = 0;
distance_old.bl = 0;


% encoder = receive(EncoderMessage);
encoder = encodervector(1);
wheelacc_old.l = -encoder.LwheelAccum;
wheelacc_old.r = encoder.RwheelAccum;

timestamp = encoder.Header.Stamp;
time_old = timestamp.Sec + timestamp.Nsec*10^(-9);

%Fetch current angle and transform quaternion coordinates to euler angle
%Calculate current yaw according to simulated position
% odom = receive(odomdata);
odom = odomvector(1);
orientation = odom.Pose.Pose.Orientation;

siny = 2.0 * (orientation.W * orientation.Z + orientation.X * orientation.Y);
cosy = +1.0 - 2.0 * (orientation.Y * orientation.Y + orientation.Z * orientation.Z); 
yaw = atan2(siny, cosy);

encoderpos_old.theta = yaw;
encoderposICC_old.theta = -yaw;
Kalman_theta = yaw;
Kalman_theta3 = yaw;
Kalman_theta5 = yaw;
firstodom.x = odom.Pose.Pose.Position.X;
firstodom.y = odom.Pose.Pose.Position.Y;



Pcov_new = 0.1*eye(2);
Pcov_new5 = 0.1*eye(2);
% Pcov_new3 = Pcov_new;
P_ukf = Pcov_new;
% uwb_total_offset = 0.1;
% uwb_variance = sqrt((uwb_total_offset*0.75));
uwb_std = 0.0458; %0.027; %0.0458;
% uwb_std = 0.027;
uwb_variance = uwb_std^2;
% uwb_std = sqrt(uwb_variance);
% uwb_std = 0.001;
%uwb_std = 0.06
% uwb_variance = sqrt(uwb_std);
uwb_kalman_position_old = [0,0]';
uwb_kalman_position_old5 = [0,0]';
uwb_kalman_position_old3 = [0,0]';

%Covariance matrixes
process_variance = 0.0016; %3e-3; %0.05
process_variance = 0.09;
R = uwb_variance * eye(4);  
Q = process_variance * eye(2);
R3 = uwb_variance^2 * eye(3);  

R5 = uwb_variance * eye(5);  

rotationoffset1 = -2* pi * 2/360;
rotationoffset2 = -rotationoffset1;


%Angle things

P_old_rot = 0.1;
filtered_angle = yaw;

true_angle = [];
fil_angle = [];
offset_angle = [];

samplerate = 2 ; %1 = 15 = 1hz ?

process_variance = 0.0012; %3e-3; %0.05 - 0.0016

% process_variance = 0.0016 * samplerate/3;
% process_variance = 0.09;
% 
R = uwb_variance * eye(4);  
Q = process_variance * eye(2);
R3 = uwb_variance^2 * eye(3);
% q = 0.001;
% Q = [(50/samplerate)^3*q/3, (50/samplerate)^2*q/2;
%     (50/samplerate)^2*q/2, (50/samplerate)*q];
P_sum = eye(2)* 0;


    for index = 2:numel(odomvector)/samplerate
        tic



        odom = odomvector(index*samplerate);
        encoder = encodervector(index*samplerate);
    %     index = index + 1;



        encoderdata = encoder;
        timestamp = encoder.Header.Stamp;
        time = timestamp.Sec + timestamp.Nsec*10^(-9);
        deltatime = time - time_old;
        time_old = time;

        %Call odometry position function



        [truepos ,encoderpos,encoderposICC, wheelacc_old1, d_tot, delta_theta] = ...
            odometry(odom, encoder,encoderpos_old, encoderposICC_old, wheelacc_old, deltatime,firstodom,samplerate);

        %Using true rotation. Switch to sensor value
%         rotationerror = (rotationoffset2-rotationoffset1)*randn+rotationoffset1;
        angle_std = 0.075;
        rotationerror = 0.05*randn*2*pi/360; %0.3375
%         rotationerror = 3.5*randn*2*pi/360; 
        rotation = truepos.theta + rotationerror;
        %rotation.new = truepos.theta +0; % rotationerror;
%         rotation = truepos.theta;
       [filtered_angle, P_old_rot] = kalmanrotation(rotation, delta_theta, filtered_angle, P_old_rot, angle_std);
        %Define old pose's
        encoderpos_old = encoderpos;
        encoderposICC_old = encoderposICC;

        true_angle = [true_angle, truepos.theta*360/(2*pi)];
        fil_angle = [fil_angle, filtered_angle*360/(2*pi)]; 
        offset_angle = [offset_angle, rotation*360/(2*pi)];

    %     rotation = filtered_angle;
%         rotation = truepos.theta;
        rotation = truepos.theta;% + rotationerror;
    %     angle = rotation*360/(2*pi)
        %Encoder vectors for plotting

        X_encoder = [X_encoder encoderpos.x];
        Y_encoder = [Y_encoder encoderpos.y];

        X_odom = [X_odom, truepos.x];
        Y_odom = [Y_odom, truepos.y];

%         X_ICC = [X_ICC, encoderposICC.x];
%         Y_ICC = [Y_ICC, encoderposICC.y];


        [d_tot, delta_theta] = encoder_change(encoderdata, wheelacc_old, deltatime);

        % Call uwb range function
        uwb_modulerange = uwb_range(truepos, uwb_std);


        % calculate the orientation angle

    %     angle = orientation_angle(truepos,uwb_total_offset);
    %     angletrue = truepos.theta * (360/(2*pi));
    %     true_orientationangle = [true_orientationangle, angletrue];
    %     orientationangle = [orientationangle, angle];

        %Call positioning functions

         z = [  uwb_modulerange.front.left.distance;
                uwb_modulerange.front.right.distance;
                uwb_modulerange.back.left.distance;
                uwb_modulerange.back.right.distance];

        z5 = [  uwb_modulerange.front.left.distance;
                uwb_modulerange.front.right.distance;
                uwb_modulerange.back.left.distance;
                uwb_modulerange.back.right.distance;
                uwb_modulerange.mid.distance];


    %     tic
        [uwb_kalman_position, Pcov_new, Kalman_theta] = uwb_pos_kalman(z, uwb_kalman_position_old, Pcov_new, encoder, wheelacc_old, deltatime,Kalman_theta,rotation, Q, R, d_tot, delta_theta);
    %     ekf_time = [ekf_time, toc];
    %     [uwb_kalman_position5, Pcov_new5, Kalman_theta5] = uwb_pos_kalman5(z5, uwb_kalman_position_old5, Pcov_new5, encoder, wheelacc_old, deltatime,Kalman_theta5,rotation, Q, R5, d_tot, delta_theta);
    P_sum = P_sum+Pcov_new;
        wheelacc_old = wheelacc_old1;


        rotation_matrix_kalman =  [ sin(rotation), cos(rotation);
                                    cos(rotation), -sin(rotation)];                 


        rover_kalman_pos = rotation_matrix_kalman * uwb_kalman_position *-1;
    %     rover_kalman_pos5 = rotation_matrix_kalman * uwb_kalman_position5 *-1;


        uwb_kalman_position_old = rover_kalman_pos;
    %     uwb_kalman_position_old5 = rover_kalman_pos;

        X_kalman = [X_kalman, rover_kalman_pos(2)];
        Y_kalman = [Y_kalman, rover_kalman_pos(1)];

    %     X_kalman5 = [X_kalman5, rover_kalman_pos5(2)];
    %     Y_kalman5 = [Y_kalman5, rover_kalman_pos5(1)];
    %     




    end
print1 = sprintf('------------------- Run %d -------------------', run);
disp(print1)
%Root mean square error (RMSE)
% 
rmse_x = sqrt(sum((X_kalman - X_odom).^2)/length(X_odom));
rmse_y = sqrt(sum((Y_kalman - Y_odom).^2)/length(Y_odom));

 test1 = sum(abs(X_kalman-X_odom))/length(X_odom);
 test2 = sum(abs(Y_kalman-Y_odom))/length(X_odom);
 avg_error = sqrt(test1^2+test2^2);
 
max_x_test = abs(X_kalman - X_odom);
max_y_test = abs(Y_kalman - Y_odom);
mean_max_x = mean(max_x_test);
mean_max_y = mean(max_y_test);

std_x = std(max_x_test);
std_y = std(max_y_test);
Errorvector = sqrt(max_x_test.^2 +max_y_test.^2);

avg_error = mean(Errorvector);

testx = abs(X_kalman-X_odom);
testy = abs(Y_kalman-Y_odom);
error1 = sqrt(testx.^2 + testy.^2);
%  for i = 1:length(error1)
%     sum = (error1(i)-avg_error)^2;
%  end

%testsum = sum(error1 - avg_error)^2

mean_error = mean(Errorvector);
error_std = std(Errorvector);
testsum = sum(error1.^2)-length(error1)*mean_error^2;
s = sqrt(testsum/(length(error1-1)));
% conf95= mean_error + 1.645*s/sqrt(length(error1));
% conf95 = 2 * sqrt(std_x^2 + std_y^2);
conf95 = prctile(Errorvector,95);
printin = sprintf('95 confidence: %.3d \nMean error: %.2d \nStandard deviation: %.2d', conf95, mean_error, error_std);
disp(printin)
rmse_x1 = abs(X_kalman-X_odom);
rmse_y1 = abs(Y_kalman-Y_odom);

net_rmse = sqrt(rmse_x^2 +rmse_y^2);

totaloffset = sqrt(rmse_x1.^2 + rmse_y1.^2);

% rmse_x5 = sqrt(sum((X_kalman5 - X_odom).^2)/length(X_odom));
% rmse_y5 = sqrt(sum((Y_kalman5 - Y_odom).^2)/length(Y_odom));
% 
% EKF5_rmse = sqrt(rmse_x5^2 +rmse_y5^2)

rmseanglefiltered = sqrt(sum((true_angle - fil_angle).^2)/length(fil_angle));
rmseangleunfil = sqrt(sum((true_angle - offset_angle).^2)/length(offset_angle));

% frac_rmse = net_rmse/net_rmse_ukf;

EKF_rmse = net_rmse;

Max_offset = sqrt(max(X_kalman - X_odom)^2 + max(Y_kalman - Y_odom)^2);
display('----------------------------------------------');
 end
%% Plotting section

close all
% 
% plot (X_encoder, Y_encoder,'r')

hold on
plot (X_odom,Y_odom,'k')
hold on

% plot(pythonposx, pythonposy)
% plot (X_lls,Y_lls,':','color',[0.5 0.5 1])
 
% hold on
plot (X_kalman,Y_kalman,'b')
hold on
% plot (X_kalman5,Y_kalman5)
plot(0,0,'*')
% 
% hold on
% plot (Xnlls, Ynlls,'r')
% % plot (X_kalman3,Y_kalman3,'r')
% hold on
% plot (X_ukf,Y_ukf,'g')
% %plot (X_ICC,Y_ICC,'g')
% %hold on
% % 
% % hold on
% % plot (testvector5,testvector6)
% % 
% % hold on
% % plot (testvector7,testvector8)
% % k=5;
% % axis([-k k -k k])
% legend ('Encoder', 'True position', 'Kalman','NLLS', 'FL','FR')
legend ('Odometry','Ground truth','EKF','Base station', 'FL','FR')
% legend ('True position','EKF','Charging station', 'FL','FR')
title('Position estimate relative to base station (0.0)', 'FontSize', 24)
xlabel('x position [m]', 'FontSize', 24)
ylabel('y position [m]', 'FontSize', 24)
daspect([1 1 1])

% cleanfigure('minimumPointsDistance', 1)
% matlab2tikz('strictFontSize',true,'gardennoenc.tex')
% % plot orientation angle
% plot(count, orientationangle, count, true_orientationangle)
% legend ('Estimated', 'True')
% title('Orientation angle')
% xlabel('measurment')
% ylabel('angle[deg]')

%%
close all
plot (X_odom,Y_odom,'*')
hold on
plot(testvector1, testvector2,'*')
hold on
plot(testvector3, testvector4,'*')
hold on
plot(testvector5, testvector6,'*')
hold on
plot(testvector7, testvector8,'*')
legend ('Odom','FRight','FLeft','BRight', 'BLeft','FR')

avg1 = sum(abs(test))/length(test);
errorrange = sqrt(abs(test.^2));
testsumerror = sum(errorrange.^2)*avg1^2;
s_test = sqrt(testsumerror/(length(errorrange-1)));
conf95= avg1 + 1.645*s_test/sqrt(length(errorrange))

%%
close all
% [f, x] = ecdf(Errorvector30enc);
% plot(x,f)
% hold on
% [f, x] = ecdf(Errorvector30noenc);
% plot(x,f)
% hold on
[f, x] = ecdf(Errorvector58enc);
plot(x,f)
hold on
[f, x] = ecdf(Errorvector58noenc);
plot(x,f)
hold on
[f, x] = ecdf(Errorvectorarbenc);
plot(x,f)
hold on
[f, x] = ecdf(Errorvectorarbnoenc);
plot(x,f)
hold on
[f, x] = ecdf(Errorvectorcircleenc);
plot(x,f)
hold on
[f, x] = ecdf(Errorvectorcirclenoenc);
plot(x,f)
% 
% hold on
% [f, x] = ecdf(Errorvectorarbenclowstd);
% plot(x,f)
% hold on
% [f, x] = ecdf(Errorvectorarbnoenclowstd);
% plot(x,f)

title('CDFs of position estimate error with larger signal standard deviation', 'FontSize', 24)
ylabel('Empirical CDF', 'FontSize', 24)
xlabel('Error [m]', 'FontSize', 24)
legend('55x55enc', '55x55noenc', 'arbenc','arbloenc' , 'circleenc', 'circlenoenc')
%legend ('30x30 enc','30x30 no enc','58x58 enc','58x58 no enc', 'arb enc','arb no enc', 'circle enc', 'circle no enc', 'arb enc low std', 'arb no enc low std')
% cleanfigure('minimumPointsDistance', 1)
% matlab2tikz('strictFontSize',true,'gardennoenc.tex')