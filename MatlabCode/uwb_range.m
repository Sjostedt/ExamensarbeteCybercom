function module = uwb_range(truepos, var)
    %UWB_RANGE estimated range to each module from start position
    %uwb_modules = uwb_range(truepos, var)
    %Input is true position of rover (x,y) and variance of distance
    %readings [m]
    %Output is estimated range to each of the module in the output struct
    global uwb_module_distance
    
    
    a = -var;
    b = var;
    armlength = sqrt(0.275^2 + 0.235^2);
    anglefront = acos(0.275/armlength);
  
    
    %x, y positions of all modules
    module.front.left.x = truepos.x + uwb_module_distance*cos(truepos.theta + pi/4);
    module.front.left.y = truepos.y + uwb_module_distance*sin(truepos.theta + pi/4);
    
    module.front.right.x = truepos.x + uwb_module_distance*cos(truepos.theta - pi/4);
    module.front.right.y = truepos.y + uwb_module_distance*sin(truepos.theta - pi/4);

    module.back.left.x = truepos.x + uwb_module_distance*cos(truepos.theta + 3*pi/4);
    module.back.left.y = truepos.y + uwb_module_distance*sin(truepos.theta + 3*pi/4 );
    
    module.back.right.x = truepos.x + uwb_module_distance*cos(truepos.theta - 3*pi/4);
    module.back.right.y = truepos.y + uwb_module_distance*sin(truepos.theta - 3*pi/4);
    %
    module.front.left.x = truepos.x + armlength*cos(truepos.theta + anglefront);
    module.front.left.y = truepos.y + armlength*sin(truepos.theta + anglefront);
    
    module.front.right.x = truepos.x + armlength*cos(truepos.theta - anglefront);
    module.front.right.y = truepos.y + armlength*sin(truepos.theta - anglefront);

    module.back.left.x = truepos.x + armlength*cos(truepos.theta + pi-anglefront);
    module.back.left.y = truepos.y + armlength*sin(truepos.theta + pi-anglefront);
    
    module.back.right.x = truepos.x + armlength*cos(truepos.theta - pi+anglefront);
    module.back.right.y = truepos.y + armlength*sin(truepos.theta - pi+anglefront);
%     
    module.mid.x = truepos.x;
    module.mid.y = truepos.y;
    
    %Range retrieved from each module
    module.front.left.distance = sqrt (module.front.left.x^2 + module.front.left.y^2) + randn*var;%(b-a)*rand+a; %randn*var
    module.front.right.distance = sqrt (module.front.right.x^2 + module.front.right.y^2)+ randn*var;%(b-a)*rand+a;
    module.back.left.distance = sqrt (module.back.left.x^2 + module.back.left.y^2)+ randn*var;%(b-a)*rand+a;
    module.back.right.distance = sqrt (module.back.right.x^2 + module.back.right.y^2)+ randn*var;%(b-a)*rand+a;
    module.mid.distance = sqrt(module.mid.x^2 + module.mid.y^2) + randn*var;
    uwb_modules= module;
    
    
end