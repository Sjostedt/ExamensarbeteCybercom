function [filtered_angle, P_new] = kalmanrotation(compassangle, delta_theta, old_angle, P_old, angle_std)
    %KALMANROTATION estimated angle of robot using compass and angle from
    %encoders

    R = angle_std *pi * 2/360;
    Q = 0.1; %1 var lite bättre
    EncoderAngleChange = delta_theta;
    
    x_hat_priori = old_angle + EncoderAngleChange;
    
    P_priori = P_old + Q;
    
    y = compassangle - x_hat_priori;
    
    S = P_priori + R;
    K = P_priori * 1/S;
    angluru = x_hat_priori + K * y;
    P_new = (1 - K) * P_priori;
    
    filtered_angle = angluru;
    
    
end