close all
rosshutdown

pause(0.5)
rosinit('10.42.0.1')         % Initiallize ROS

PositionMessage = rossubscriber('/uwb_position');

t0 = clock;
X = [];
Y = [];

time = 60;
while etime(clock, t0) < time
    position = receive(PositionMessage);
    X = [X, position.X];
    Y = [Y, position.Y];
end

plot(X(3:end),Y(3:end))