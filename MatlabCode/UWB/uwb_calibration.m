%%
rosshutdown
clc
clear all
close all 
rosinit('10.42.0.1') 
pause(1)

uwb = rossubscriber('/uwb_distance');


close all
t0 = clock;
n= 0;
alpha = 0.1;

Hz = 50;
Ts = 1/Hz;

Range_movingaverage = [];
Range_unfiltered = [];
Range_exponentialfilter = [];
Range_WMA = [];
Range_unfiltered = [];
CMA = [];
Range_tustin = [];

time = 2*60;
uwb_receive = receive(uwb);
uwb_range = uwb_receive.Range;

tustin_old = uwb_range;
value_old = uwb_range;
fc = 4; %200 hz cutoff
Wc = 2 * pi * fc;
k = 0
%Range_WMA = [uwb_range]
% while etime(clock, t0) < time
while k<500
    tic
   
    uwb_receive = receive(uwb);
    uwb_range = uwb_receive.Range;
    
    %Exponential filter
    if n == 0
%         uwb_range_exp = uwb_range;
%         CMA = [CMA, uwb_range];
        Range_unfiltered = [Range_unfiltered, uwb_range];
        Range_tustin = [Range_tustin,uwb_range];
        %Range_WMA =[Range_WMA, uwb_range];
        n = 1;
        
    elseif uwb_range > 2*Range_unfiltered(end)
        Range_unfiltered = [Range_unfiltered, Range_unfiltered(end)];
        Range_tustin = [Range_tustin, Range_tustin(end)];
        %
    elseif uwb_range < Range_unfiltered -1
        Range_unfiltered = [Range_unfiltered, Range_unfiltered(end)];
        Range_tustin = [Range_tustin, Range_tustin(end)];
    else
        Range_unfiltered = [Range_unfiltered, uwb_range];
%         uwb_range_exp = alpha * uwb_range +(1-alpha) * Range_exponentialfilter(end);
        tustin_old = (uwb_range * Wc * Ts + Wc * Ts * value_old - tustin_old * (Wc * Ts - 2))/(Wc * Ts + 2);
        Range_tustin = [Range_tustin, tustin_old];
        value_old = uwb_range;
%         if uwb_range_exp > 2*Range_exponentialfilter(end)
%             uwb_range_exp = Range_exponentialfilter(end);
%             k = 1
%         end
%         if uwb_range_exp < Range_exponentialfilter(end)-0.4
%             uwb_range_exp = Range_exponentialfilter(end);
%             l = 1
%         end
    end
    k = k +1
end
   %Moving average POP append ?
  
%    CMAlen = numel(CMA);
%    CMA_1 = (uwb_range + CMAlen * CMA(end))/(1+CMAlen);
%    CMA =[CMA, CMA_1];
   
   %Weighted moving average
%    weightedsamples = 4;
%    if numel(Range_WMA) == 0
%         testvector = [1*uwb_range]; 
%    elseif numel(Range_WMA) < weightedsamples
%         testvector = [numel(Range_WMA)*uwb_range];
%    else
%         testvector = [weightedsamples*uwb_range];
%    end
%     h = 0;
%     for i = 1:numel(Range_WMA)
%         if i>weightedsamples-1
%             break
%         end
%         h = h+ 1;
%         testvector = [testvector, (weightedsamples-i)*Range_WMA(end-i+1)];      
%     end
%     numbersamples = numel(testvector);
%     testsum = sum(testvector)/(numbersamples*(numbersamples + 1)/2);
%     
    %First order tustin
    
    
    
%     value_old = uwb_range;
    %Range_tustin = [Range_tustin, tustin_old];
%     Range_WMA = [Range_WMA, testsum];
%     Range_movingaverage = [Range_movingaverage, CMA_1];
%     Range_exponentialfilter = [Range_exponentialfilter, uwb_range_exp];
    hej = 1;
    while toc < Ts
        hej = hej + 1
    end
% end

true_value = 0.4;

step = time/numel(Range_unfiltered);
y = [0:step:time-step];
%%
close all
outliervector = isoutlier(Range_unfiltered);
for i = 1:numel(Range_unfiltered)-1
    if outliervector(i) == 1
        Range_unfiltered(i) = Range_unfiltered(i-1);
    end
end

plot(y,Range_unfiltered,'b')
hold on
%plot(y,Range_exponentialfilter, 'r')
%hold on
%plot(y,Range_tustin, 'g')
%hold on
%plot(y,Range_movingaverage)

%plot(y,Range_WMA)
%hold on

%hold on
%refline(0,true_value);
mean_unfiltered = mean(Range_unfiltered)
var_range = var(Range_unfiltered);
std_range = sqrt(var_range)
%mean_tustin = mean(Range_tustin)
%legend ('Unfiltered','1st order Tustin','Reference', 'Exponential smoothing')
title('Unfiltered readings from UWB module','FontSize', 20)
xlabel('Time [s]','FontSize', 20)
ylabel('Range [m]', 'FontSize', 20)
% 
% % histogram plots
figure
hist(Range_unfiltered, 10)
title('Histogram of range measurements','FontSize', 20)
ylabel('Samples', 'FontSize', 20)
xlabel('Range [m]', 'FontSize', 20)