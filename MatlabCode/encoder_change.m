function [d_tot, delta_theta] = encoder_change(encoderdata, wheelacc_old, deltatime)
    %ENCODER_CHANGE outputs total distance travelled since last sample an
    %change of rotation
    global uwb_module_distance
    global wheeldiameter
    global wheelbase
    global encoderpulse
    
    
    %Calculate pulses since last sample
    lwheelacc = -encoderdata.LwheelAccum;
    rwheelacc = encoderdata.RwheelAccum;
    r_pulses = rwheelacc - wheelacc_old.r;
    l_pulses = lwheelacc - wheelacc_old.l;
    
    
    if r_pulses>1
        r_pulses = r_pulses + poissrnd(1);
    end
        
    if l_pulses>1
        l_pulses = l_pulses + poissrnd(1);
    end
    
    
    if r_pulses < -2
          r_pulses = r_pulses - poissrnd(1);
    end
    
    if l_pulses < -2
        l_pulses = l_pulses - poissrnd(1);
    end
     %Calculate number of revolutions and the corresponding distance
    r_revolutions = r_pulses / encoderpulse;            %Revolutions
    l_revolutions = l_pulses / encoderpulse;
    r_distance = pi * wheeldiameter * r_revolutions;    %Distance travelled right wheel
    l_distance = pi * wheeldiameter * l_revolutions;    % Distance travelled left wheel
    
    % Total distance travelled since last sample
    d_tot = (r_distance + l_distance)/2; 
    
    %Change of rotation
    delta_theta = (r_distance - l_distance)/wheelbase;
    
    encoder_change = [d_tot, delta_theta];