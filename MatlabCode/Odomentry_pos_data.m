%% Odometry
clc, clear all

%% Define variables
global wheeldiameter
global wheelbase
global encoderpulse
global Ts

odompos = struct;
encoderpos = struct;
encoderposICC = struct;
encoderpos_old = struct;
encoderposICC_old = struct;
uwb_modulerange = struct;
rotation = struct;

wheeldiameter= 0.24;% Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946;
wheelbase = 0.4645;    % Wheel base [m]
encoderpulse =  349; %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / pi;

%% Rosinit
rosinit('10.42.0.1') 

%% Subscribe to stuff
EncoderMessage = rossubscriber('/wheel_encoder');
odomdata = rossubscriber('/odom');
pause(2);
Ts = 0.2;
%% Sample loop
clc;
%Define some variables
t0 = clock;
n = 1; % Initial value for if

% Initial positions
X_encoder = [];
Y_encoder = [];

X_odom = [];
Y_odom = [];

X_ICC = [];
Y_ICC = [];

odompos.x = 0;
odompos.y = 0;
odompos.theta = 0;
encoderpos.x = 0;
encoderpos.y = 0;
encoderpos.theta = 0;
encoderposICC.x = 0;
encoderposICC.y = 0;
encoderposICC.theta = 0;
encoderpos_old = encoderpos;
encoderposICC_old = encoderposICC;

encoder = receive(EncoderMessage);
wheelacc_old.l = -encoder.LwheelAccum;
wheelacc_old.r = encoder.RwheelAccum;
true_angle = [];
timestamp = encoder.Header.Stamp;
time_old = timestamp.Sec + timestamp.Nsec*10^(-9);

%Fetch current angle and transform quaternion coordinates to euler angle
%Calculate current yaw according to simulated position
odom = receive(odomdata);
orientation = odom.Pose.Pose.Orientation;

siny = 2.0 * (orientation.W * orientation.Z + orientation.X * orientation.Y);
cosy = +1.0 - 2.0 * (orientation.Y * orientation.Y + orientation.Z * orientation.Z); 
yaw = atan2(siny, cosy);

encoderpos_old.theta = 0;
encoderposICC_old.theta = -0;

firstodom.x = odom.Pose.Pose.Position.X;
firstodom.y = odom.Pose.Pose.Position.Y;

while etime(clock, t0) < 180
    tic
    
   % Receive messages and calculate delta time
    odom = receive(odomdata);
    encoder = receive(EncoderMessage);

    % Timestamps
    timestamp = encoder.Header.Stamp;
    time = timestamp.Sec + timestamp.Nsec*10^(-9);
    time_old = time;
    deltatime = time - time_old;
    
    %Call odometry position function
    [truepos ,encoderpos, encoderposICC, wheelacc_old1] = ...
        odometry(odom, encoder, encoderpos_old, encoderposICC_old, wheelacc_old, deltatime, firstodom);
    
    %Define old pose's
    encoderpos_old = encoderpos;
    encoderposICC_old = encoderposICC;
    
    true_angle = [true_angle, truepos.theta];
    
    %Encoder vectors for plotting
    X_encoder = [X_encoder encoderpos.x];
    Y_encoder = [Y_encoder encoderpos.y];
    
    X_odom = [X_odom, truepos.x];
    Y_odom = [Y_odom, truepos.y];
    
    X_ICC = [X_ICC, encoderposICC.x];
    Y_ICC = [Y_ICC, encoderposICC.y];
  
    tic    
    wheelacc_old = wheelacc_old1;
    pausing = 1;
    
    while toc < Ts
        pausing = pausing + 1;
    end

end
k = 1
%% Plotting section
close all
plot (X_encoder, Y_encoder,'r')
title('ENCODER POS.')
xlabel('x [m]')
ylabel('y [m]')

% plot(X_odom, Y_odom)
% title('Odometry POS.')
% xlabel('x [m]')
% ylabel('y [m]')
% 
% plot(X_ICC, Y_ICC)
% title('ICC POS.')
% xlabel('x [m]')
% ylabel('y [m]')



