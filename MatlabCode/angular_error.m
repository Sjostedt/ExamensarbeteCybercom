%% Angular offset due to variation in UWB ranging
clear all, close all, clc

%% Variables
% General distances
format long
d1 = 0.55;
armlength = sqrt(0.275^2 + 0.235^2);
anglefront = acosd(0.275/armlength);
d2 = 0.4;
orientationangle = [];

%% placement of nodes on BS
BSnode1 = 0.20;
BSnode2 = -0.20;
truepos.x = 0;
truepos.y = 20;

% placement of nodes on MS
% module 1
module.front.left.x1 = truepos.x + armlength * cos(anglefront) + BSnode1;
module.front.left.y1 = truepos.y + armlength * sin(anglefront) ;
module.front.left.x2 = truepos.x + armlength * cos(anglefront) + BSnode2;
module.front.left.y2 = truepos.y + armlength * sin(anglefront);

% module 2
module.back.left.x1 = truepos.x +  armlength * cos(pi-anglefront) + BSnode1;
module.back.left.y1 = truepos.y + armlength * sin(pi-anglefront);  
module.back.left.x2 = truepos.x + armlength * cos(pi-anglefront) + BSnode2;
module.back.left.y2 = truepos.y + armlength * sin(pi-anglefront);

% distance 1,2 for first node
module.front.left.distance1 = sqrt (module.front.left.x1^2 + module.front.left.y1^2);
module.front.left.distance2 = sqrt (module.front.left.x2^2 + module.front.left.y2^2);

% distance 1,2 for second node
module.back.left.distance1 = sqrt (module.back.left.x1^2 + module.back.left.y1^2);
module.back.left.distance2 = sqrt (module.back.left.x2^2 + module.back.left.y2^2);

% offset vectors
offset_vector = -0.10:0.005:0.10;
% add offset to the calc. distances
module.front.left.distance1 = module.front.left.distance1 + offset_vector;
module.front.left.distance2 = module.front.left.distance2 + offset_vector;

module.back.left.distance1 = module.back.left.distance1 + offset_vector;
module.back.left.distance2 = module.back.left.distance2 + offset_vector;

%% Calculated angle
a = length(offset_vector);
orientationangle = [];
x = [];
for i = 1:a
%     fistcos = (l1^2 + module.front.left.distance1(i)^2 - module.back.left.distance1(a+1-i)^2)/(2*l1*module.front.left.distance1(i)^2)
%     secondcos = (module.front.left.distance1(i)^2+d2^2-module.front.left.distance2((a+1-i))^2)/(2*module.front.left.distance1(i)*d2)

    orientation = acosd((d1^2 + module.front.left.distance1(i)^2 - module.back.left.distance1(a+1-i)^2)/(2*d1*module.front.left.distance1(i)^2))...
     -90 + acosd((module.front.left.distance1(i)^2+d2^2-module.front.left.distance2((a+1-i))^2)/(2*module.front.left.distance1(i)*d2))
    y = (module.front.left.distance1(i)-module.front.left.distance1(42-i));
    x = [x, y];
%     orientation = acosd((d1^2 + module.front.left.distance1^2 - module.back.left.distance1^2)/(2*d1*module.front.left.distance1^2))...
%       -90 + acosd((module.front.left.distance1^2+d2^2-module.front.left.distance2^2)/(2*module.front.left.distance1*d2))
    orientationangle = [orientationangle, orientation];
end

%% Plot variation in angle
close all;

c = ones(1,a) + 89;
orientationangle = orientationangle-0.4;

plot(x, orientationangle, x, c)
legend('Estimated angle','True angle')
axis([-0.21 0.21 55 125])
title('Error in yaw angle')
xlabel('Offset in ranging distances [m]')
ylabel('Orientation angle [deg]')
