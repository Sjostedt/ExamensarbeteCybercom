%Subscribe and fetch sum data
%Install ROS for matlab
%Install dependencies for custom ROS messages "Robotics System Toolbox
%Interface for ROS Custom Message" 

clc
clear all
%% Define stuff
% Define types
global wheeldiameter
global wheelbase
global encoderpulse
global Ts
global uwb_module_distance

odompos = struct;
encoderpos = struct;
encoderposICC = struct;
encoderpos_old = struct;
encoderposICC_old = struct;
uwb_modulerange = struct;
rotation = struct;

% Define constants
wheeldiameter= 0.24;% Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946;
wheelbase = 0.4645;    % Wheel base [m]
wheelbase = 0.47;    % Wheel base [m]
encoderpulse =  349; %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / pi;
Hz = 80;
Ts = 1/Hz;
uwb_module_distance = 0.3;

%%
%rosinit('10.42.0.1')         % Initiallize ROS
rosinit()
%Only needs to be done ONCE
% driverpath = fullfile('/','home','micke','ExamensarbeteCybercom','catkin_ws','src', 'hrp'); % Specify path of custom drivers
%rosgenmsg(driverpath); % Load and generate custom msg packages
%addpath('/home/lensund/ExamensarbeteCybercom/catkin_ws/src/hrp/matlab_gen/msggen')
% Follow output of rosgenmsg
%% Subscribe to stuff
EncoderMessage = rossubscriber('/wheel_encoder');
odomdata = rossubscriber('/odom');


pause(2);

%% For Python confirmation
PythonPos = rossubscriber('Encoder_pos');
%% Sample loop
%Define some variables
t0 = clock;
n = 1; % Initial value for if
c = 0;
% count = [];
% true_orientationangle = [];
% orientationangle = [];

% Initial positions
X_encoder = [];
Y_encoder = [];

X_odom = [];
Y_odom = [];

X_ICC = [];
Y_ICC = [];

X_kalman = [];
Y_kalman = [];

X_kalman3 = [];
Y_kalman3 = [];

X_lls = [];
Y_lls = [];

X_ukf = [];
Y_ukf = [];

% pythonposx = [];
% pythonposy = [];

odompos.x = 0;
odompos.y = 0;
odompos.theta = 0;
encoderpos.x = 0;
encoderpos.y = 0;
encoderpos.theta = 0;
encoderposICC.x = 0;
encoderposICC.y = 0;
encoderposICC.theta = 0;
encoderpos_old = encoderpos;
encoderposICC_old = encoderposICC;

distance_old.fr = 0;
distance_old.fl = 0;
distance_old.br = 0;
distance_old.bl = 0;


encoder = receive(EncoderMessage);
wheelacc_old.l = -encoder.LwheelAccum;
wheelacc_old.r = encoder.RwheelAccum;

timestamp = encoder.Header.Stamp;
time_old = timestamp.Sec + timestamp.Nsec*10^(-9);

%Fetch current angle and transform quaternion coordinates to euler angle
%Calculate current yaw according to simulated position
odom = receive(odomdata);
orientation = odom.Pose.Pose.Orientation;

siny = 2.0 * (orientation.W * orientation.Z + orientation.X * orientation.Y);
cosy = +1.0 - 2.0 * (orientation.Y * orientation.Y + orientation.Z * orientation.Z); 
yaw = atan2(siny, cosy);

encoderpos_old.theta = yaw;
encoderposICC_old.theta = -yaw;
Kalman_theta = yaw;
Kalman_theta3 = yaw;

firstodom.x = odom.Pose.Pose.Position.X;
firstodom.y = odom.Pose.Pose.Position.Y;

testvector1 = [];
testvector2 = [];
testvector3 = [];
testvector4 = [];
testvector5 = [];
testvector6 = [];
testvector7 = [];
testvector8 = [];

Xnlls = [];
Ynlls = [];

ukf_time = [];
ekf_time = [];

Pcov_new = 0.1*eye(2);
% Pcov_new3 = Pcov_new;
P_ukf = Pcov_new;
% uwb_total_offset = 0.1;
% uwb_variance = sqrt((uwb_total_offset*0.75));
uwb_variance = 2.1e-3;
uwb_std = sqrt(uwb_variance);
% uwb_std = 0.001;
%uwb_std = 0.06
% uwb_variance = sqrt(uwb_std);
uwb_kalman_position_old = [0,0]';
uwb_kalman_position_old3 = [0,0]';

%Covariance matrixes
process_variance = 9e-4; %0.05
R = uwb_variance * eye(4);  
Q = process_variance * eye(2);
R3 = uwb_variance^2 * eye(3);  

uwb_ukf_position = [0;0];
rotationoffset1 = -2* pi * 2/360;
rotationoffset2 = -rotationoffset1;
old_nlls=[0,0]';

%Angle things

P_old_rot = 0.1;
filtered_angle = yaw;

true_angle = [];
fil_angle = [];
offset_angle = [];

odomvector = [];
encodervector = [];
% while etime(clock, t0) < 30 

while 1  
    tic
    
   % Receive messages and calculate delta time
    odom = receive(odomdata);
    encoder = receive(EncoderMessage);
    odomvector = [odomvector, odom];
    encodervector = [encodervector, encoder];





    
    encoderdata = encoder;
    timestamp = encoder.Header.Stamp;
    time = timestamp.Sec + timestamp.Nsec*10^(-9);
    deltatime = time - time_old;
    time_old = time;
    
    %Call odometry position function
    
  
    samplerate = 10;
    [truepos ,encoderpos,encoderposICC, wheelacc_old1, d_tot, delta_theta] = ...
        odometry(odom, encoder,encoderpos_old, encoderposICC_old, wheelacc_old, deltatime,firstodom,samplerate);
    
    %Using true rotation. Switch to sensor value
    rotationerror = (rotationoffset2-rotationoffset1)*randn+rotationoffset1;
    angle_std = 2*0.3375;
    rotationerror = angle_std*randn*2*pi/360; %0.3375
    rotation = truepos.theta + rotationerror;
    %rotation.new = truepos.theta +0; % rotationerror;
%     rotation = truepos.theta;
   [filtered_angle, P_old_rot] = kalmanrotation(rotation, delta_theta, filtered_angle, P_old_rot, angle_std);
    %Define old pose's
    encoderpos_old = encoderpos;
    encoderposICC_old = encoderposICC;
    
    true_angle = [true_angle, truepos.theta*360/(2*pi)];
    fil_angle = [fil_angle, filtered_angle*360/(2*pi)]; 
    offset_angle = [offset_angle, rotation*360/(2*pi)];
    
%     rotation = filtered_angle;
    rotation = truepos.theta;
%     rotation = truepos.theta + rotationerror;
%     angle = rotation*360/(2*pi)
    %Encoder vectors for plotting
    
    X_encoder = [X_encoder encoderpos.x];
    Y_encoder = [Y_encoder encoderpos.y];
    
    X_odom = [X_odom, truepos.x];
    Y_odom = [Y_odom, truepos.y];
    x_true = truepos.x
    y_true = truepos.y
    
    X_ICC = [X_ICC, encoderposICC.x];
    Y_ICC = [Y_ICC, encoderposICC.y];
    
    
    [d_tot, delta_theta] = encoder_change(encoderdata, wheelacc_old, deltatime);
    
    % Call uwb range function
    uwb_modulerange = uwb_range(truepos, uwb_std);
%     uwb_modulerange3 = uwb_range3(truepos,uwb_total_offset);
    testvector1 = [testvector1, uwb_modulerange.front.right.x];
    testvector2 = [testvector2, uwb_modulerange.front.right.y];
    
    testvector3 = [testvector3, uwb_modulerange.front.left.x];
    testvector4 = [testvector4, uwb_modulerange.front.left.y];
    
    testvector5 = [testvector5, uwb_modulerange.back.right.x];
    testvector6 = [testvector6, uwb_modulerange.back.right.y];
    
    testvector7 = [testvector7, uwb_modulerange.back.left.x];
    testvector8 = [testvector8, uwb_modulerange.back.left.y];
    
    % calculate the orientation angle

%     angle = orientation_angle(truepos,uwb_total_offset);
%     angletrue = truepos.theta * (360/(2*pi));
%     true_orientationangle = [true_orientationangle, angletrue];
%     orientationangle = [orientationangle, angle];
    
    %Call positioning functions
    
     z = [  uwb_modulerange.front.left.distance;
            uwb_modulerange.front.right.distance;
            uwb_modulerange.back.left.distance;
            uwb_modulerange.back.right.distance];
    
%     z3 = [  uwb_modulerange3.front.mid.distance;
%             uwb_modulerange3.back.left.distance;
%             uwb_modulerange3.back.right.distance];
    
    [uwb_ukf_position, P_ukf, X1] = uwb_pos_ukf(uwb_ukf_position,P_ukf,z,Q,R, rotation);
    ukf_time = [ukf_time, toc];
    
    
    [uwb_kalman_position, Pcov_new, Kalman_theta] = uwb_pos_kalman(z, uwb_kalman_position_old, Pcov_new, encoder, wheelacc_old, deltatime,Kalman_theta,rotation, Q, R, d_tot, delta_theta);
    ekf_time = [ekf_time, toc];
%     [uwb_kalman_position3, Pcov_new3, Kalman_theta3] = uwb_pos_kalman3(z3, uwb_kalman_position_old3, Pcov_new3, encoder, wheelacc_old, deltatime,Kalman_theta3,truepos, Q, R3, d_tot, delta_theta);
    
    [uwb_lls_position,distance_old] = uwb_pos_lls(uwb_modulerange, distance_old);
    
    uwb_nlls_pos = uwb_pos_nlls(z, old_nlls);
    old_nlls = uwb_nlls_pos;
    
    wheelacc_old = wheelacc_old1;
   
    rover_lls_pos = uwb_lls_position;
    
    
    rotation_matrix_lls = [ sin(rotation), cos(rotation);
                        cos(rotation), -sin(rotation)];

    rotation_matrix_kalman =  [ sin(rotation), cos(rotation);
                                cos(rotation), -sin(rotation)];                 
                            
    rover_nlls_pos = rotation_matrix_lls*uwb_nlls_pos * -1;                        
                            
    rover_lls_pos = rotation_matrix_lls * rover_lls_pos * -1; 
    
    rover_kalman_pos = rotation_matrix_kalman * uwb_kalman_position *-1;
    
%     rover_kalman_pos3 = rotation_matrix_kalman * uwb_kalman_position3 *-1;
  
%     uwb_kalman_position_old3 = rover_kalman_pos3;
    
    uwb_kalman_position_old = rover_kalman_pos;
%     uwb_kalman_position_old = uwb_kalman_position;
    uwb_ukf_position_rot = rotation_matrix_kalman* uwb_ukf_position * -1;
   
    X_ukf = [X_ukf, uwb_ukf_position_rot(2)];
    Y_ukf = [Y_ukf, uwb_ukf_position_rot(1)];
    
    X_lls = [X_lls, rover_lls_pos(2)];
    Y_lls = [Y_lls, rover_lls_pos(1)];
    
    Xnlls = [Xnlls, rover_nlls_pos(2)];
    Ynlls = [Ynlls, rover_nlls_pos(1)];

    X_kalman = [X_kalman, rover_kalman_pos(2)];
    Y_kalman = [Y_kalman, rover_kalman_pos(1)];

    pausing = 1;
    while toc < Ts
        pausing = pausing + 1;
    end


end

%Root mean square error (RMSE)
% 
rmse_x = sqrt(sum((X_kalman - X_odom).^2)/length(X_odom));
rmse_y = sqrt(sum((Y_kalman - Y_odom).^2)/length(Y_odom));

net_rmse = sqrt(rmse_x^2 +rmse_y^2);

rmse_x3 = sqrt(sum((X_ukf - X_odom).^2)/length(X_odom));
rmse_y3 = sqrt(sum((Y_ukf - Y_odom).^2)/length(Y_odom));

net_rmse_ukf = sqrt(rmse_x3^2 +rmse_y3^2)

rmseanglefiltered = sqrt(sum((true_angle - fil_angle).^2)/length(fil_angle))
rmseangleunfil = sqrt(sum((true_angle - offset_angle).^2)/length(offset_angle))

frac_rmse = net_rmse/net_rmse_ukf;

EKF_rmse = net_rmse
rmse_x = sqrt(sum((Xnlls - X_odom).^2)/length(X_odom));
rmse_y = sqrt(sum((Ynlls - Y_odom).^2)/length(Y_odom));

NLLS_rmse = sqrt(rmse_x^2 +rmse_y^2)

rmse_x = sqrt(sum((X_lls - X_odom).^2)/length(X_odom));
rmse_y = sqrt(sum((Y_lls - Y_odom).^2)/length(Y_odom));

LLS_rmse = sqrt(rmse_x^2 +rmse_y^2);
Max_offset = sqrt(max(X_kalman - X_odom)^2 + max(Y_kalman - Y_odom)^2)

%% Plotting section

close all
% 
plot (X_encoder, Y_encoder,'r')

hold on
plot (X_odom,Y_odom,'k')
hold on

% plot(pythonposx, pythonposy)
% plot (X_lls,Y_lls,':','color',[0.5 0.5 1])
 
% hold on
plot (X_kalman,Y_kalman,'b')
% 
% hold on
% plot (Xnlls, Ynlls,'r')
% % plot (X_kalman3,Y_kalman3,'r')
% hold on
% plot (X_ukf,Y_ukf,'g')
% %plot (X_ICC,Y_ICC,'g')
% %hold on
% % 
% % hold on
% % plot (testvector5,testvector6)
% % 
% % hold on
% % plot (testvector7,testvector8)
% % k=5;
% % axis([-k k -k k])
% legend ('Encoder', 'True position', 'Kalman','NLLS', 'FL','FR')
legend ('Odom','True position','EKF','NLLS', 'FL','FR')

title('Position relative to basestation (0.0)', 'FontSize', 24)
xlabel('x position [m]', 'FontSize', 24)
ylabel('y position [m]', 'FontSize', 24)


% % plot orientation angle
% plot(count, orientationangle, count, true_orientationangle)
% legend ('Estimated', 'True')
% title('Orientation angle')
% xlabel('measurment')
% ylabel('angle[deg]')

%%
close all
plot (X_odom,Y_odom,'*')
hold on
plot(testvector1, testvector2,'*')
hold on
plot(testvector3, testvector4,'*')
hold on
plot(testvector5, testvector6,'*')
hold on
plot(testvector7, testvector8,'*')
legend ('Odom','FRight','FLeft','BRight', 'BLeft','FR')
