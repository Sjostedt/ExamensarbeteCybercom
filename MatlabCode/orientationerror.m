% clear all; close all; clc
close all
uwb_module_distance = 0.20;
x = [2];
y = [2];
orientation1 = [];
l1 = 0.4;
l2 = 2;

for i = 1:1
    % x, y positions of all modules
    module.front.left.x1 = x(i) - uwb_module_distance + 1;
    module.front.left.y1 = y(i);
    
    module.front.left.x2 = x(i) - uwb_module_distance - 1;
    module.front.left.y2 = y(i);   
    
    module.back.left.x1 = x(i) + uwb_module_distance + 1;
    module.back.left.y1 = y(i);
    
    module.back.left.x2 = x(i) + uwb_module_distance - 1;
    module.back.left.y2 = y(i);

    %Range retrieved from each module
    module.front.left.distance1 = -0.1+sqrt (module.front.left.x1^2 + module.front.left.y1^2);
    module.front.left.distance2 = -0.1+sqrt (module.front.left.x2^2 + module.front.left.y2^2);
    module.back.left.distance1 = 0.1+sqrt (module.back.left.x1^2 + module.back.left.y1^2);
    module.back.left.distance2 = 0.1+sqrt (module.back.left.x2^2 + module.back.left.y2^2);
    
    angle1 = acosd((l1^2 + module.front.left.distance2^2 - module.back.left.distance2^2) / (2 * l1 * module.front.left.distance2));
    angle2 = acosd((module.front.left.distance1^2 + module.front.left.distance2^2 - l2^2) / (2 * module.front.left.distance1 * module.front.left.distance2));
    angle3 = acosd((l2^2 + module.front.left.distance1^2 - module.front.left.distance2^2) / (2 * l2 * module.front.left.distance1));
    angle4 = angle3 - 90;

    orientation = (angle1 + angle2 + angle4);
    orientation1 = [orientation1, orientation];
    
end

