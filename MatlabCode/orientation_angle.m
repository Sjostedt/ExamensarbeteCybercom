function [orientation_filtered, orientation, orientation_old, orientation_old1, orientation_old2] = orientation_angle(truepos, var, orientation_old , orientation_old1, orientation_old2)

    % ORIENTATION_ANGLE estimates the angle of the lawn mower relative the
    % real systems y-axis
    format long
    
    %% Initiate varaiabels
  
    % Module distance stuff
    global uwb_module_distance
%     a = -var;
%     b = var;

    % General stuff
    l1 = sqrt(0.275^2 + 0.235^2) * cos(pi/4) * 2;
    BSnode1 = -0.20;
    BSnode1 = -0.20; 
    armlength = sqrt(0.275^2 + 0.235^2);
    anglefront = acos(0.275/armlength);
    
    % Tustin stuff
    Hz = 10;
    Ts = 1/Hz;
    Wc = 2 * pi * 0.5;


    %% Position of all modules
    % x, y positions of all modules
    % Module 1
    module.front.left.x1 = truepos.x + uwb_module_distance * cos(truepos.theta + pi/4 );
    module.front.left.y1 = truepos.y + uwb_module_distance * sin(truepos.theta + pi/4) + (l2/2);
    module.front.left.x2 = truepos.x + uwb_module_distance * cos(truepos.theta + pi/4);
    module.front.left.y2 = truepos.y + uwb_module_distance * sin(truepos.theta + pi/4) - (l2/2);

%     module.front.left.x1 = truepos.x + armlength * cos(truepos.theta + anglefront);
%     module.front.left.y1 = truepos.y + armlength * sin(truepos.theta + anglefront) + (l2/2);
%     module.front.left.x2 = truepos.x + armlength * cos(truepos.theta + anglefront);
%     module.front.left.y2 = truepos.y + armlength * sin(truepos.theta + anglefront) - (l2/2);
   
    % Module 2
    module.front.right.x1 = truepos.x + uwb_module_distance * cos(truepos.theta - pi/4);
    module.front.right.y1 = truepos.y + uwb_module_distance * sin(truepos.theta - pi/4) + (l2/2);
    module.front.right.x2 = truepos.x + uwb_module_distance * cos(truepos.theta - pi/4);
    module.front.right.y2 = truepos.y + uwb_module_distance * sin(truepos.theta - pi/4) - (l2/2);  

%     module.front.right.x1 = truepos.x + armlength * cos(truepos.theta - anglefront);
%     module.front.right.y1 = truepos.y + armlength * sin(truepos.theta - anglefront) + (l2/2);
%     module.front.right.x2 = truepos.x + armlength * cos(truepos.theta - anglefront);
%     module.front.right.y2 = truepos.y +  armlength * sin(truepos.theta - anglefront) - (l2/2);   

    % Module 3
    module.back.left.x1 = truepos.x + uwb_module_distance * cos(truepos.theta + 3*pi/4);
    module.back.left.y1 = truepos.y + uwb_module_distance * sin(truepos.theta + 3*pi/4 ) + (l2/2);
    module.back.left.x2 = truepos.x + uwb_module_distance * cos(truepos.theta + 3*pi/4);
    module.back.left.y2 = truepos.y + uwb_module_distance * sin(truepos.theta + 3*pi/4 ) - (l2/2);

%     module.back.left.x1 = truepos.x +  armlength * cos(truepos.theta + pi-anglefront);
%     module.back.left.y1 = truepos.y + armlength * sin(truepos.theta + pi-anglefront) + (l2/2);  
%     module.back.left.x2 = truepos.x + armlength * cos(truepos.theta + pi-anglefront);
%     module.back.left.y2 = truepos.y + armlength * sin(truepos.theta + pi-anglefront) - (l2/2);

    % Module 4
    module.back.right.x1 = truepos.x + uwb_module_distance * cos(truepos.theta - 3*pi/4);
    module.back.right.y1 = truepos.y + uwb_module_distance * sin(truepos.theta - 3*pi/4) + (l2/2);
    module.back.right.x2 = truepos.x + uwb_module_distance * cos(truepos.theta - 3*pi/4);
    module.back.right.y2 = truepos.y + uwb_module_distance * sin(truepos.theta - 3*pi/4) - (l2/2);

%     module.back.right.x1 = truepos.x + armlength*cos(truepos.theta - pi+anglefront);
%     module.back.right.y1 = truepos.y + armlength*sin(truepos.theta - pi+anglefront) + (l2/2); 
%     module.back.right.x2 = truepos.x + armlength * cos(truepos.theta - pi+anglefront);
%     module.back.right.y2 = truepos.y + armlength * sin(truepos.theta - pi+anglefront) - (l2/2);
    
    %% Range retrieved from each module
    % Module 1
    module.front.left.distance1 = sqrt (module.front.left.x1^2 + module.front.left.y1^2);% + sqrt(7.6*10^(-4))*randn;
    module.front.left.distance2 = sqrt (module.front.left.x2^2 + module.front.left.y2^2);% + sqrt(7.6*10^(-4))*randn;

    % Module 2
    module.front.right.distance1 = sqrt (module.front.right.x1^2 + module.front.right.y1^2);% + sqrt(7.6*10^(-4))*randn;
    module.front.right.distance2 = sqrt (module.front.right.x2^2 + module.front.right.y2^2);% + sqrt(7.6*10^(-4))*randn;
    
    % Module 3
    module.back.left.distance1 = sqrt (module.back.left.x1^2 + module.back.left.y1^2);% + sqrt(7.6*10^(-4))*randn;
    module.back.left.distance2 = sqrt (module.back.left.x2^2 + module.back.left.y2^2);% + sqrt(7.6*10^(-4))*randn;

    % Module 4
    module.back.right.distance1 = sqrt (module.back.right.x1^2 + module.back.right.y1^2);% + sqrt(7.6*10^(-4))*randn;
    module.back.right.distance2 = sqrt (module.back.right.x2^2 + module.back.right.y2^2);% + sqrt(7.6*10^(-4))*randn;
    
    % Shorter names for the conditions
    fl1 = module.front.left.distance1;
    fl2 = module.front.right.distance2;
    fr1 = module.back.left.distance1;
    fr2 = module.back.right.distance1;
    bl1 = module.front.right.distance1;
    bl2 = module.back.right.distance1;
    br1 = module.back.right.distance1;
    br2 = module.back.right.distance2;
    
    % Calculate the orientation angle
   
    if fl1 < fr1 &&  bl2 < br2 % || abs(fl1+bl1) < abs(fr1+br1) 
        angle1 = acosd((l1^2 + module.front.left.distance2^2 - module.back.left.distance2^2) / (2 * l1 * module.front.left.distance2));
        angle2 = acosd((module.front.left.distance1^2 + module.front.left.distance2^2 - l2^2) / (2 * module.front.left.distance1 * module.front.left.distance2));
        angle3 = acosd((l2^2 + module.front.left.distance1^2 - module.front.left.distance2^2) / (2 * l2 * module.front.left.distance1));
        angle4 = 180 - angle3;
        angle5 = 180 - 90 - angle4;
        q = 10
        orientation = -(angle1 + angle2 + angle5);
        
        % Tustin
        orientation_filtered = (Wc * Ts * orientation + Wc * Ts * orientation_old - orientation_old1 * (Wc * Ts - 2))/(Wc * Ts + 2);
        orientation_old = orientation;
        orientation_old1 = orientation_filtered;
        orientation_old2 = orientation_old1;
        
        % Moving avrage 4
%         orientation_moving = (1/4)*(orientation + orientation_old + orientation_old1 + orientation_old2);
%         orientation_old = orientation;
%         orientation_old1 = orientation_old;
%         orientation_old2 = orientation_old1;
        
    elseif fr2 < fl2 && br1 < bl1 % || abs(fr1+br1) < abs(fl1+bl1) 
        angle1 = acosd((l1^2 + module.back.left.distance2^2 - module.front.left.distance2^2) / (2 * l1 * module.back.left.distance2));
        angle2 = acosd((module.back.left.distance1^2 + module.back.left.distance2^2 - l2^2) / (2 * module.back.left.distance1 * module.back.left.distance2));
        angle3 = acosd((l2^2 + module.back.left.distance1^2 - module.back.left.distance2^2) / (2 * l2 * module.back.left.distance1));
        angle4 = 180 - angle3;
        angle5 = 180 - 90 - angle4;
        q = 20
        orientation = (angle1 + angle2 + angle5);
        
        % Tustin
        orientation_filtered = (Wc * Ts * orientation + Wc * Ts * orientation_old - orientation_old1 * (Wc * Ts - 2))/(Wc * Ts + 2);
        orientation_old = orientation;
        orientation_old1 = orientation_filtered;
        orientation_old2 = orientation_old1;
        
    else %abs(fl1+fl2+fr1+fr2) < abs(bl1+bl2+br1+br2) ||  abs(fl1+fl2+fr1+fr2) > abs(bl1+bl2+br1+br2)
        angle1 = acosd((l1^2 + module.back.left.distance2^2 - module.back.right.distance2^2) / (2 * l1 * module.back.left.distance2));
        angle2 = acosd((module.back.left.distance1^2 + module.back.left.distance2^2 - l2^2) / (2 * module.back.left.distance1 * module.back.left.distance2));
        angle3 = acosd((l2^2 + module.back.left.distance1^2 - module.back.left.distance2^2) / (2 * l2 * module.back.left.distance1));
        angle4 = 180 - angle3;
        angle5 = 180 - angle4;
        q = 30
        orientation = 180 -(angle1 + angle2 + angle5);
        if orientation < -180
            orientation = (orientation + 360);
        end
        % Tustin
        orientation_filtered = (Wc * Ts * orientation + Wc * Ts * orientation_old - orientation_old1 * (Wc * Ts - 2))/(Wc * Ts + 2);
        orientation_old = orientation;
        orientation_old1 = orientation_filtered;
        orientation_old2 = orientation_old1;
    
    end
    
end
