%%
rosshutdown
clc
clear all
close all 
rosinit('10.42.0.1') 
pause(2)

bearing_data = rossubscriber('/bearing_publish');
%%

close all
t0 = clock;
n= 0;
alpha = 0.1;

Hz = 50;
Ts = 1/Hz;

bearing = [];

time = 5;
% uwb_receive = receive(uwb);
% uwb_range = uwb_receive.Range;
% 
% tustin_old = uwb_range;
% value_old = uwb_range;
% fc = 4; %200 hz cutoff
% Wc = 2 * pi * fc;

%Range_WMA = [uwb_range]
while etime(clock, t0) < time
    tic
    
    bearing_receive = receive(bearing_data);
    angle = bearing_receive.Bearing;
    bearing = [bearing, angle];
end

    hej = 1;
    while toc < Ts
        hej = hej + 1;
    end
% end

true_value = 0.4;

step = time/numel(bearing);
y = [0:step:time-step];
%%
close all
plot(y,bearing)

%legend ('Unfiltered','1st order Tustin','Reference', 'Exponential smoothing')
title('Readings from compass','FontSize', 20)
xlabel('Time [s]','FontSize', 20)
ylabel('Angle [deg]', 'FontSize', 20)
% 
% % histogram plots
% figure
% hist(Range_unfiltered, 20)
% title('Histogram of range measurements','FontSize', 20)
% ylabel('Samples', 'FontSize', 20)
% xlabel('Range [m]', 'FontSize', 20)