%% Ranging statsistics.
close all; clc

format long
% Data.
x.unfiltered = Range_unfiltered;
x.mean = mean(Range_unfiltered);
x.length = length(x.unfiltered);
t = 1.960; %



%% Konfidencintervall for range
sum1 = 0;
for i = 1:x.length
    sum = (x.unfiltered(i)-x.mean)^2;
    sum1 = sum1 + sum
end

s = sqrt(sum/(x.length-1));


x.min = x.mean - t * s *(1/sqrt(x.length));
x.max = x.mean + t * s *(1/sqrt(x.length))
x.mean
x.var = var(Range_unfiltered)
