close all, clear all, clc
% Load measured range
%% variables


x.unfiltered = Range_unfiltered;
x.mean = mean(Range_unfiltered);
x.length = length(x.unfiltered);
t = 1.960; %
a = 0.07;               % range of disturbance a and b

m = length(Range_unfiltered);        % number fo measurments
a = 0.07;% range of disturbance
variance = var(Range_unfiltered);
standard_deviation = std(Range_unfiltered);

b = -0.07;
l = 43.4;                % actual distance when the ranging was performed
steps = 1:x.length;

signal_array = ones(1,x.length)*l;   % vector for the signal
simulated_signal = [];               % simulated signal after adding disturbance

% Simulated signal


% add disturbance on the signal
j = 0;
for i = 1:x.length
    j = signal_array(i) + sqrt(variance) * randn;
    simulated_signal = [simulated_signal, j];
end

% determine a konfidence intervall for the unfiltered signal
sum = 0;
for i = 1:x.length
    sum = sum + (x.unfiltered(i)-x.mean)^2;
end
s = sqrt(sum/(x.length-1));
x.min = x.mean - t * s *(1/sqrt(x.length));
x.max = x.mean + t * s *(1/sqrt(x.length));
x.var = var(Range_unfiltered)

% determine a confidence intervall for the unfiltered signal
sum = 0;
signal_mean = mean(simulated_signal);
for i = 1:x.length
    sum = sum + (simulated_signal(i)-signal_mean)^2;
end


out = [];

for i = 1:m
    test = signal_array(i) + randn * standard_deviation;
    out = [out, test];
end
out = []
for i = 1:m
    out1 = signal_array(i) + randn * standard_deviation;
    out = [out, out1];

end
% 
% s = sqrt(sum/(x.length-1));
% test.min = x.mean - t * s *(1/sqrt(x.length));
% test.max = x.mean + t * s *(1/sqrt(x.length));
% test.var = var(simulated_signal)

%% Plot

i = 1:x.length;
pu = i;



% plot(x, simulated_signal, x, Range_unfiltered);
% axis([1 x.length 8.4 9])

i = 1:x.length, pu = i;

plot(pu, simulated_signal)
% axis([1 x.length 8.4 9])

hold on
plot( pu, Range_unfiltered);
title('Simulated and Real UWB signal')
xlabel('Measurements')
ylabel('[m]')
legend('Simulated UWB signal','Unfiltered UWB signal')

% cleanfigure('minimumPointsDistance', 1)
% matlab2tikz('strictFontSize',true,'gardennoenc.tex')

%%
close all
simulated_signal1 = abs(simulated_signal-43.4);
Range_unfiltered1 = abs(Range_unfiltered-43.4);
[b1,b2]=ecdf(simulated_signal1);
[a1,a2]=ecdf(Range_unfiltered1);
plot(a2,a1)
hold on
plot(b2,b1)
legend('Simulated signal', 'Real signal')
xlabel('Range error [m]')
ylabel('Empirical CDF')
title('CDF of measurement error of simulated and real signal')
%%

plot(x, out, x, Range_unfiltered);
axis([1 m 8.58 8.82])

figure
plot(pu, out);
hold on
plot(pu, Range_unfiltered, 'r');
% axis([1 m 8.58 8.82])

hold on
title('Simulated and measured UWB signal','FontSize', 20)
xlabel('Measurements','FontSize', 20)
ylabel('Range [m]', 'Fontsize', 20)
legend('Simulated UWB signal','Unfiltered UWB signal')
mean_unfiltered = mean(Range_unfiltered)
mean_simulated = mean(out)

figure
hist(Range_unfiltered,10)
title('Measured signal','FontSize', 20)
xlabel('Range [m]','FontSize', 20)
ylabel('Measurements', 'Fontsize', 20)

figure
hist(out,10)
title('Simulated signal','FontSize', 20)
xlabel('Range [m]','FontSize', 20)
ylabel('Measurements', 'Fontsize', 20)

