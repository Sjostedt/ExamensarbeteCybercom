# Local positioning system using UWB modules and Husqvarna automover

Master thesis work on local positioning system solely using UWB modules in a inverted topology on the robotic lawnmover from Husqvarna

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Softwares required for the project:
Matlab
Matlab ROS toolbox
Python
Robotic Operating System
RPi
Arduino

Hardware:
The software is designed for the UWB modules DWM1000 developed by Decawave where 4 anchors are running on the RPi while a tag polls and sends messages through the Arduino. Messages are fetched through the ROS network on the Automover.
```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Mikael Sjöstedt** - *Initial work* - [Sjostedt](https://gitlab.com/Sjostedt)

* **Filip Lensund** - *Initial work* - [Lensund](https://gitlab.com/lensund)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
