#!/usr/bin/env python


import smbus
import time

import rospy

from compass_publisher.msg import angle
from std_msgs.msg import String

import thread##

bus = smbus.SMBus(1)
address = 0x60

def bearing255():
        bear = bus.read_byte_data(address, 1)
        return bear

def bearing3599():
        bear1 = bus.read_byte_data(address, 2)
        bear2 = bus.read_byte_data(address, 3)
        bear = (bear1 << 8) + bear2
        bear = bear/10.0
        return bear

def BearingThread():
  get_bearing()
  rospy.spin()

#thread.start_new_thread( BearingThread, ( ) )
#time.sleep(0.1)

while True:
        bearing = bearing3599()     #this returns the value to 1 decimal place in degrees.
        #bear255 = bearing255()      #this returns the value as a byte between 0 and 255.
        print bearing
        #print bear255
        time.sleep(0.1)
        pub = rospy.Publisher('bearing_publish', angle, queue_size=1)
        rospy.init_node('compass_node', anonymous=True)
        #rate = rospy.Rate(200) # 40hz
        msg = angle()
        msg.bearing = bearing

        pub.publish(msg)
        #rate.sleep()
