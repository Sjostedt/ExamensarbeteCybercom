#!/usr/bin/env python
#Dis tha main
import DW1000
import monotonic
import DW1000Constants as C
import rospy

from DW1000RangingAnchor_class import *
from uwb_publisher.msg import distance
from std_msgs.msg import String

DEBUG = 0

CS1 = 16
IRQ1 = 19

CS2 = 20
IRQ2 = 26

CS3 = 21
IRQ3 = 13

CS4 = 12
IRQ4 = 6

CS5 = 23
IRQ5 = 22

range_module1_old = 0


M1delay = 16330
M2delay = 16332
M3delay = 16317
M4delay = 16333


DWM1000_module = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1, M1delay)
#DWM1000_module = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2, M2delay)
#DWM1000_module = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3, M3delay)
#DWM1000_module = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4, M4delay)

#Add 5th module if applied
#DWM1000_module = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)

def das_loop():

    DEBUG = 1

    pub = rospy.Publisher('uwb_distance', distance, queue_size=1)
    rospy.init_node('DWM1000_main', anonymous=True)
    rate = rospy.Rate(40) # 40hz
    msg = distance()
    #Loop for calculating the range
    msg.module = 'module1'
    #msg.name = 'test'
    #troll = 'hej'
    test = True
    while test:
        range_module1 = DWM1000_module.loop()
        if range_module1 != None:
            #rangemodule1_old = range_module1
            test = False

    #range_module2 = DWM1000_module.loop()
    #range_module3 = DWM1000_module.loop()
    #range_module4 = DWM1000_module.loop()
    #print('Range module 5: %.3f' %(range_module1))
    msg.range = range_module1

    #while not rospy.is_shutdown():

    if DEBUG == 1:
        rospy.loginfo(msg)

    pub.publish(msg)
    rate.sleep()
    rangemodule1_old = None
    #test = True






    #except KeyboardInterrupt:
        #DW1000.close()


def main():
    try:
        while 1:
            das_loop()
    except KeyboardInterrupt:
        print('Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
