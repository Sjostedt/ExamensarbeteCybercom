classdef distance < robotics.ros.Message
    %distance MATLAB implementation of uwb_publisher/distance
    %   This class was automatically generated by
    %   robotics.ros.msg.internal.gen.MessageClassGenerator.
    
    %   Copyright 2014-2018 The MathWorks, Inc.
    
    %#ok<*INUSD>
    
    properties (Constant)
        MessageType = 'uwb_publisher/distance' % The ROS message type
    end
    
    properties (Constant, Hidden)
        MD5Checksum = '83f2d3ae4b9285bc707ca0f585f4f7b8' % The MD5 Checksum of the message definition
    end
    
    properties (Access = protected)
        JavaMessage % The Java message object
    end
    
    properties (Dependent)
        Module
        Range
    end
    
    properties (Constant, Hidden)
        PropertyList = {'Module', 'Range'} % List of non-constant message properties
        ROSPropertyList = {'module', 'range'} % List of non-constant ROS message properties
    end
    
    methods
        function obj = distance(msg)
            %distance Construct the message object distance
            import com.mathworks.toolbox.robotics.ros.message.MessageInfo;
            
            % Support default constructor
            if nargin == 0
                obj.JavaMessage = obj.createNewJavaMessage;
                return;
            end
            
            % Construct appropriate empty array
            if isempty(msg)
                obj = obj.empty(0,1);
                return;
            end
            
            % Make scalar construction fast
            if isscalar(msg)
                % Check for correct input class
                if ~MessageInfo.compareTypes(msg(1), obj.MessageType)
                    error(message('robotics:ros:message:NoTypeMatch', obj.MessageType, ...
                        char(MessageInfo.getType(msg(1))) ));
                end
                obj.JavaMessage = msg(1);
                return;
            end
            
            % Check that this is a vector of scalar messages. Since this
            % is an object array, use arrayfun to verify.
            if ~all(arrayfun(@isscalar, msg))
                error(message('robotics:ros:message:MessageArraySizeError'));
            end
            
            % Check that all messages in the array have the correct type
            if ~all(arrayfun(@(x) MessageInfo.compareTypes(x, obj.MessageType), msg))
                error(message('robotics:ros:message:NoTypeMatchArray', obj.MessageType));
            end
            
            % Construct array of objects if necessary
            objType = class(obj);
            for i = 1:length(msg)
                obj(i,1) = feval(objType, msg(i)); %#ok<AGROW>
            end
        end
        
        function module = get.Module(obj)
            %get.Module Get the value for property Module
            module = char(obj.JavaMessage.getModule);
        end
        
        function set.Module(obj, module)
            %set.Module Set the value for property Module
            validateattributes(module, {'char'}, {}, 'distance', 'Module');
            
            obj.JavaMessage.setModule(module);
        end
        
        function range = get.Range(obj)
            %get.Range Get the value for property Range
            range = single(obj.JavaMessage.getRange);
        end
        
        function set.Range(obj, range)
            %set.Range Set the value for property Range
            validateattributes(range, {'numeric'}, {'nonempty', 'scalar'}, 'distance', 'Range');
            
            obj.JavaMessage.setRange(range);
        end
    end
    
    methods (Access = protected)
        function cpObj = copyElement(obj)
            %copyElement Implements deep copy behavior for message
            
            % Call default copy method for shallow copy
            cpObj = copyElement@robotics.ros.Message(obj);
            
            % Create a new Java message object
            cpObj.JavaMessage = obj.createNewJavaMessage;
            
            % Iterate over all primitive properties
            cpObj.Module = obj.Module;
            cpObj.Range = obj.Range;
        end
        
        function reload(obj, strObj)
            %reload Called by loadobj to assign properties
            obj.Module = strObj.Module;
            obj.Range = strObj.Range;
        end
    end
    
    methods (Access = ?robotics.ros.Message)
        function strObj = saveobj(obj)
            %saveobj Implements saving of message to MAT file
            
            % Return an empty element if object array is empty
            if isempty(obj)
                strObj = struct.empty;
                return
            end
            
            strObj.Module = obj.Module;
            strObj.Range = obj.Range;
        end
    end
    
    methods (Static, Access = {?matlab.unittest.TestCase, ?robotics.ros.Message})
        function obj = loadobj(strObj)
            %loadobj Implements loading of message from MAT file
            
            % Return an empty object array if the structure element is not defined
            if isempty(strObj)
                obj = robotics.ros.custom.msggen.uwb_publisher.distance.empty(0,1);
                return
            end
            
            % Create an empty message object
            obj = robotics.ros.custom.msggen.uwb_publisher.distance;
            obj.reload(strObj);
        end
    end
end
