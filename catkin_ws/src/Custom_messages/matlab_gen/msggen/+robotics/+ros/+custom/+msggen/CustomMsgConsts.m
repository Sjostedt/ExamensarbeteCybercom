classdef CustomMsgConsts
    %CustomMsgConsts This class stores all message types
    %   The message types are constant properties, which in turn resolve
    %   to the strings of the actual types.
    
    %   Copyright 2014-2018 The MathWorks, Inc.
    
    properties (Constant)
        compass_publisher_AddTwoInts = 'compass_publisher/AddTwoInts'
        compass_publisher_AddTwoIntsRequest = 'compass_publisher/AddTwoIntsRequest'
        compass_publisher_AddTwoIntsResponse = 'compass_publisher/AddTwoIntsResponse'
        compass_publisher_angle = 'compass_publisher/angle'
        odom_simu_AddTwoInts = 'odom_simu/AddTwoInts'
        odom_simu_AddTwoIntsRequest = 'odom_simu/AddTwoIntsRequest'
        odom_simu_AddTwoIntsResponse = 'odom_simu/AddTwoIntsResponse'
        odom_simu_odomposition = 'odom_simu/odomposition'
        position_publisher_AddTwoInts = 'position_publisher/AddTwoInts'
        position_publisher_AddTwoIntsRequest = 'position_publisher/AddTwoIntsRequest'
        position_publisher_AddTwoIntsResponse = 'position_publisher/AddTwoIntsResponse'
        position_publisher_position = 'position_publisher/position'
        uwb_publisher_AddTwoInts = 'uwb_publisher/AddTwoInts'
        uwb_publisher_AddTwoIntsRequest = 'uwb_publisher/AddTwoIntsRequest'
        uwb_publisher_AddTwoIntsResponse = 'uwb_publisher/AddTwoIntsResponse'
        uwb_publisher_distance = 'uwb_publisher/distance'
    end
    
    methods (Static, Hidden)
        function messageList = getMessageList
            %getMessageList Generate a cell array with all message types.
            %   The list will be sorted alphabetically.
            
            persistent msgList
            if isempty(msgList)
                msgList = cell(12, 1);
                msgList{1} = 'compass_publisher/AddTwoIntsRequest';
                msgList{2} = 'compass_publisher/AddTwoIntsResponse';
                msgList{3} = 'compass_publisher/angle';
                msgList{4} = 'odom_simu/AddTwoIntsRequest';
                msgList{5} = 'odom_simu/AddTwoIntsResponse';
                msgList{6} = 'odom_simu/odomposition';
                msgList{7} = 'position_publisher/AddTwoIntsRequest';
                msgList{8} = 'position_publisher/AddTwoIntsResponse';
                msgList{9} = 'position_publisher/position';
                msgList{10} = 'uwb_publisher/AddTwoIntsRequest';
                msgList{11} = 'uwb_publisher/AddTwoIntsResponse';
                msgList{12} = 'uwb_publisher/distance';
            end
            
            messageList = msgList;
        end
        
        function serviceList = getServiceList
            %getServiceList Generate a cell array with all service types.
            %   The list will be sorted alphabetically.
            
            persistent svcList
            if isempty(svcList)
                svcList = cell(4, 1);
                svcList{1} = 'compass_publisher/AddTwoInts';
                svcList{2} = 'odom_simu/AddTwoInts';
                svcList{3} = 'position_publisher/AddTwoInts';
                svcList{4} = 'uwb_publisher/AddTwoInts';
            end
            
            % The message list was already sorted, so don't need to sort
            % again.
            serviceList = svcList;
        end
        
        function actionList = getActionList
            %getActionList Generate a cell array with all action types.
            %   The list will be sorted alphabetically.
            
            persistent actList
            if isempty(actList)
                actList = cell(0, 1);
            end
            
            % The message list was already sorted, so don't need to sort
            % again.
            actionList = actList;
        end
    end
end
