package odom_simu;

public interface AddTwoInts$Service extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "odom_simu/AddTwoInts$Service";
  static final java.lang.String _DEFINITION = "int64 a\nint64 b\n---\nint64 sum\n";
  static final boolean _IS_SERVICE = true;
  static final boolean _IS_ACTION = false;
}
