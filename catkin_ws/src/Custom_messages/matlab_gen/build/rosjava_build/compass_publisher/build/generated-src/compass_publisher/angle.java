package compass_publisher;

public interface angle extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "compass_publisher/angle";
  static final java.lang.String _DEFINITION = "float32 bearing\n";
  static final boolean _IS_SERVICE = false;
  static final boolean _IS_ACTION = false;
  float getBearing();
  void setBearing(float value);
}
