package compass_publisher;

public interface AddTwoIntsRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "compass_publisher/AddTwoIntsRequest";
  static final java.lang.String _DEFINITION = "int64 a\nint64 b\n";
  static final boolean _IS_SERVICE = true;
  static final boolean _IS_ACTION = false;
  long getA();
  void setA(long value);
  long getB();
  void setB(long value);
}
