package uwb_publisher;

public interface AddTwoIntsResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "uwb_publisher/AddTwoIntsResponse";
  static final java.lang.String _DEFINITION = "int64 sum";
  static final boolean _IS_SERVICE = true;
  static final boolean _IS_ACTION = false;
  long getSum();
  void setSum(long value);
}
