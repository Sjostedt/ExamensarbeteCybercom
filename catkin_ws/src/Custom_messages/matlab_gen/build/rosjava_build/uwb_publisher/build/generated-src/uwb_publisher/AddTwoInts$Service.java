package uwb_publisher;

public interface AddTwoInts$Service extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "uwb_publisher/AddTwoInts$Service";
  static final java.lang.String _DEFINITION = "int64 a\nint64 b\n---\nint64 sum\n";
  static final boolean _IS_SERVICE = true;
  static final boolean _IS_ACTION = false;
}
