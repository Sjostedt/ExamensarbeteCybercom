package uwb_publisher;

public interface distance extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "uwb_publisher/distance";
  static final java.lang.String _DEFINITION = "string module\nfloat32 range\n";
  static final boolean _IS_SERVICE = false;
  static final boolean _IS_ACTION = false;
  java.lang.String getModule();
  void setModule(java.lang.String value);
  float getRange();
  void setRange(float value);
}
