package position_publisher;

public interface position extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "position_publisher/position";
  static final java.lang.String _DEFINITION = "float32 X\nfloat32 Y\n";
  static final boolean _IS_SERVICE = false;
  static final boolean _IS_ACTION = false;
  float getX();
  void setX(float value);
  float getY();
  void setY(float value);
}
