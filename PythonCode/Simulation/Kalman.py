

import numpy as np
import math

global wheeldiameter
global wheelbase
global encoderpulse
#global uwb_module_distance



def KalmanPosition(z, position_old, P_old, DeltaTime, Q, R, ModulePositions):
    F = np.eye(2)
    B = 0 * np.eye(2)

    x_hat_priori = F * position_old + B#*controlsignal

    P_priori = F*P_old*np.transpose(F) + Q

    x_hat_priori_x = x_hat_priori(0)
    x_hat_priori_y = x_hat_priori(1)

    # Module positions
    #Front left
    module_1x =  ModulePositions(1,1)
    module_1y =  ModulePositions(1,2)
    #Front right
    module_2x =    ModulePositions(2,1)
    module_2y =    ModulePositions(2,2)
    #Back left
    module_3x =    ModulePositions(3,1)
    module_3y =    ModulePositions(3,2)
    #Back right
    module_4x =    ModulePositions(4,1)
    module_4y =    ModulePositions(4,2)


    h_priori = np.array([[math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))],
                [math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))],
                [math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))],
                [math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))]])

    H = np.array([[(x_hat_priori_x - module_1x)/math.sqrt(math.pow(x_hat_priori_x - module_1.x,2) + math.pow(x_hat_priori_y - module_1y,2)), (x_hat_priori_y - module_1y)/math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))]
         [(x_hat_priori_x - module_2x)/math.sqrt(math.pow(x_hat_priori_x - module_2.x,2) + math.pow(x_hat_priori_y - module_2y,2)), (x_hat_priori_y - module_2y)/math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))]
         [(x_hat_priori_x - module_3x)/math.sqrt(math.pow(x_hat_priori_x - module_3.x,2) + math.pow(x_hat_priori_y - module_3y,2)), (x_hat_priori_y - module_3y)/math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))]
         [(x_hat_priori_x - module_4x)/math.sqrt(math.pow(x_hat_priori_x - module_4.x,2) + math.pow(x_hat_priori_y - module_4y,2)), (x_hat_priori_y - module_4y)/math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))]])

    y = z - h_priori

    S = H * P_priori * np.transpose(H) + R
    Kalman_gain = P_priori * np.transpose(H) * np.linalg.inv(S)

   #Outputs
    uwb_position = x_hat_priori + Kalman_gain * y
    P_new = [np.eye(2) - Kalman_gain * H]*P_priori

    return uwb_position, P_new
