"""
Main module for the positioning system
This python module contains low-level functions to interact with the DW1000 chip using a Raspberry Pi 3. It requires the following modules:
math, time, spidev, Rpi.GPIO, random etc - FILL IN.
"""

#!/usr/bin/env python
#import time
#import DW1000
import monotonic
import DW1000Constants as C
import rospy
#from DW1000RangingAnchor_class import *
import numpy as np
import math
import thread##
from am_driver.msg import WheelEncoder
import time ##

from Kalman import *

from odom_simu.msg import odomposition
from std_msgs.msg import String


#Define RPi output pins for the UWB modules
CS1 = 16
CS2 = 20
CS3 = 21
CS4 = 12
CS5 = 23
IRQ1 = 19
IRQ2 = 26
IRQ3 = 13
IRQ4 = 6
IRQ5 = 22

MaxSamples = 100000
moduleHz = 0.5 #maximum allowed samplingrate per module
moduleTs = 1/moduleHz
StartupTime = 0.5

ModulesRunning = False

"""
    Define variables
"""
#Rover specific

global wheeldiameter
global wheelbase
global encoderpulse
#global uwb_module_distance
wheeldiameter= 0.24 # Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946 #Distance traveled per tic
wheelbase = 0.4645    # Wheel base [m]
encoderpulse =  349 # %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / math.pi
uwb_module_distance = 0.3

#Kalman specific
Pcov_new = 0.1 * np.eye(2)
uwb_total_offset = 0.1
process_variance = 0.05
uwb_variance = math.sqrt((uwb_total_offset*0.75));
R = math.pow(uwb_variance,2) * np.eye(4)
Q = process_variance * np.eye(2)
UwbKalmanPos = np.transpose([0,0])

# Module placement

# Module positions
#Front left
module_1x =  uwb_module_distance/math.sqrt(2)
module_1y =  uwb_module_distance/math.sqrt(2)
#Front right
module_2x =    uwb_module_distance/math.sqrt(2)
module_2y =    -uwb_module_distance/math.sqrt(2)
#Back left
module_3x =    -uwb_module_distance/math.sqrt(2)
module_3y =    uwb_module_distance/math.sqrt(2)
#Back right
module_4x =    -uwb_module_distance/math.sqrt(2)
module_4y =    -uwb_module_distance/math.sqrt(2)
u = True;

ModulePositions = [[module_1x, module_1y], [module_2x, module_2y], [module_3x, module_3y], [module_4x, module_4y]]
def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """
    return int(round(monotonic.monotonic() * C.MILLISECONDS))

#Random
TimeOld = millis()
if ModulesRunning:
    # Create instance of all modules
    DWM1000_module1 = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1)
    print('\n')
    time.sleep(StartupTime)
    DWM1000_module2 = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2)
    print('\n')
    time.sleep(StartupTime)
    DWM1000_module3 = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3)
    time.sleep(StartupTime)
    print('\n')
    DWM1000_module4 = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4)
    time.sleep(StartupTime)
    print('\n')
    time.sleep(StartupTime)

#time.sleep(0.006)
#Add 5th module if applied
#DWM1000_module5 = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)

# def OdometryNode(data):
#      #rospy.loginfo("%f is age: %f" % (data.lwheelAccum, data.rwheelAccum))
#      #print(data)
#      global lwheelAccum
#      lwheelAccum = data.lwheelAccum
#      return(data.lwheelAccum)
#      OdomSubscriber.unregister()

class odometry():

    def __init__(self):
        self.xpose = 0
        self.ypose = 0
        self.LWheelAccum = None
        self.RWheelAccum = None

    def get_pose(self):
<<<<<<< HEAD
        self.pose = rospy.Subscriber("/wheel_encoder", WheelEncoder, self.OdometryCallback, None)
=======
        self.pose = rospy.Subscriber("/wheel_encoder", WheelEncoder, self.OdometryCallback, queue_size=1)
>>>>>>> bc2693a422802277beacfc35d3ad81168da4968d

    def OdometryCallback(self, data):
        self.LWheelAccum = data.lwheelAccum
        self.RWheelAccum = data.rwheelAccum
        #print(data)
        rospy.wait_for_shutdown()
        #self.pose.unregister()

def OdometryCallback(data):
    global LWheelAccumthread, RWheelAccumthread
    LWheelAccumthread = data.lwheelAccum
    RWheelAccumthread = data.rwheelAccum

def get_pose():
    pose = rospy.Subscriber("/wheel_encoder", WheelEncoder, OdometryCallback, queue_size=1)

# Define a function for the thread
def OdomThread():
  get_pose()
  rospy.spin()


def OdometryPose(OdomData, OldOdomData, Omega, OldPos):
    """Calculates position using odometry"""
    global encoderpulse, wheelbase

    OldPosX = OldPos[0]
    OldPosY = OldPos[1]
    WheelAccOld_R = OldOdomData[0]
    WheelAccOld_L = OldOdomData[1]
    R_pulses = OdomData[0] - WheelAccOld_R
    L_pulses = OdomData[1] - WheelAccOld_L

    R_Revolutions = R_pulses / encoderpulse
    L_Revolutions = -L_pulses / encoderpulse

    R_Distance = math.pi * wheeldiameter * R_Revolutions
    L_Distance = math.pi * wheeldiameter * L_Revolutions

    OmegaDelta = (R_Distance-L_Distance)/wheelbase

    Omega = Omega + OmegaDelta
    TotalDistance = (R_Distance + L_Distance)/2

    Xpos =  OldPosX + TotalDistance * math.cos(Omega)
    Ypos =  OldPosY + TotalDistance * math.sin(Omega)

    OldOdom = [OdomData[0], OdomData[1]]
    return Xpos, Ypos, Omega, OldOdom


def das_loop(OldOdom, Omega, OldPos):
    global RWheelAccumthread, LWheelAccumthread

    DeltaTime = millis() - TimeOld
    #Loop for calculating the range

    if ModulesRunning:
        """
            Loop for all modules. Returns range to tag or tries of MaxSamples
        """
        range_module1 = None
        range_module2 = None
        range_module3 = None
        range_module4 = None

        """
            Module 1 Run
        """
        i = 0
        test1 = True
        while (test1) and (i<MaxSamples):
            range_module1 = DWM1000_module1.loop()
            i = i + 1
            if range_module1 != None:

                print('Range module 1: %.3f' %(range_module1))
                test1 = False

        """
            Module 2 Run
        """
        i = 0
        test2 = True
        while (test2) and (i<MaxSamples):
            range_module2 = DWM1000_module2.loop()
            i = i + 1
            if range_module2 != None:
                print('Range module 2: %.3f' %(range_module2))
                test2 = False

        """
            Module 3 Run
        """
        i = 0
        test3 = True
        while (test3) and (i<MaxSamples):
            range_module3 = DWM1000_module3.loop()
            i = i + 1
            if range_module3 != None:
                print('Range module 3: %.3f' %(range_module3))
                test3 = False

        """
            Module 4 Run
        """
        i = 0
        test4 = True

        while (test4) and (i<MaxSamples):
           range_module4 = DWM1000_module4.loop()
           i = i + 1

           if range_module4 != None:
                test4 = False
                print('Range module 4: %.3f' %(range_module4))
        """
            End of module loops
        """

        z = [range_module1, range_module2, range_module3, range_module4]
        #if MeasuredRange != None:
        if any(x is None for x in z) == False:
            UwbKalmanPos, Pcov_new = uwb_pos_kalman(z, UwbKalmanPos, Pcov_new, DeltaTime, Q, R, ModulePositions)

        print(UwbKalmanPos)

<<<<<<< HEAD
    #OdomSubscriber = No, queue_size=Nonene
    #OdomSubscriber = rospy.Subscriber("/wheel_encoder", WheelEncoder, OdometryNode)
    #print(lwheelAccum)
    #rospy.spin()
    #while int(round(odom.LWheelAccum)) == int(round(WheelAccOld_L)):
    i = 1
    #while i<10:
        #Subscribe and fetch accumulated wheelturns
    odom.get_pose()
    #print(odom.LWheelAccum)
    #print('lwheelaccum: %.4f \nrwheelaccum: %.4f' %(odom.LWheelAccum, odom.RWheelAccum))

    print('Do other stuff \n \n')
    #print(OldPos)
    Xpos, Ypos, Omega = OdometryPose(odom, OldOdom, Omega, OldPos)
    OldPos = np.matrix([Xpos, Ypos])
    OldOdom = [odom.LWheelAccum, odom.RWheelAccum]
    #print('Xpos: %f \nYpos: %f' %(Xpos, Ypos))
    print(odom.LWheelAccum)

=======

    odom = [RWheelAccumthread, LWheelAccumthread]
    #print(Omega)
    Xpos, Ypos, Omega, OldOdom = OdometryPose(odom, OldOdom, Omega, OldPos)



    OldPos = [Xpos, Ypos]



    """ Publisher specific"""
    rate = rospy.Rate(40) #Remove ?
    pub = rospy.Publisher('Encoder_pos', odomposition, queue_size=1)
    msg = odomposition()
    msg.X = Xpos
    msg.Y = Ypos

    pub.publish(msg)
    #rospy.loginfo(msg)
    rate.sleep()



    # if range_module1 != None:
    #     print('Range module 1: %.3f' %(range_module1))
    # if range_module2 != None:
    #     print('Range module 2: %.3f' %(range_module2))
    # if range_module3 != None:
    #     print('Range module 3: %.3f' %(range_module3))
    # if range_module4 != None:
    #     print('Range module 4: %.3f' %(range_module4))

    return OldOdom, Omega, OldPos
>>>>>>> bc2693a422802277beacfc35d3ad81168da4968d
def main():
    try:
        global RWheelAccumthread, LWheelAccumthread
        rospy.init_node('EncoderListener', anonymous=True)

        #rospy.init_node('DWM1000_main', anonymous=True)


        thread.start_new_thread( OdomThread, ( ) )
        time.sleep(0.1)

        # Get initial pose
        OldOdom = [RWheelAccumthread, LWheelAccumthread]
        Omega = 0
        OldPos = [0, 0]
        while 1:
<<<<<<< HEAD
            das_loop(odom, OldOdom, Omega, OldPos)

=======
            OldOdom, Omega, OldPos = das_loop(OldOdom, Omega, OldPos)
            #print(OldOdom)
            #print(Omega)
            #print(OldPos)
        print('This should never happen')
>>>>>>> bc2693a422802277beacfc35d3ad81168da4968d
    except KeyboardInterrupt:
        print('Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
