#Dis tha main

#import time
#import DW1000
import monotonic
import DW1000Constants as C
import rospy
from DW1000RangingAnchor_class import *

#Define RPi output pins for the UWB modules
CS1 = 16
CS2 = 20
CS3 = 21
CS4 = 12
CS5 = 23
IRQ1 = 19
IRQ2 = 26
IRQ3 = 13
IRQ4 = 6
IRQ5 = 22

MaxSamples = 100000
moduleHz = 0.5 #maximum allowed samplingrate per module
moduleTs = 1/moduleHz
StartupTime = 0.5

# Create instance of all modules
DWM1000_module1 = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1)
print('\n')
time.sleep(StartupTime)
DWM1000_module2 = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2)
print('\n')
time.sleep(StartupTime)
DWM1000_module3 = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3)
time.sleep(StartupTime)
print('\n')
DWM1000_module4 = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4)
time.sleep(StartupTime)
print('\n')
#time.sleep(StartupTime)
#GPIO.output(CS3, GPIO.LOW)
#GPIO.output(CS4, GPIO.LOW)
#time.sleep(0.006)
#Add 5th module if applied
#DWM1000_module5 = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)

def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """
    return int(round(monotonic.monotonic() * C.MILLISECONDS))

def das_loop():
    #Loop for calculating the range


    """
        Loop for all modules. Returns range to tag or tries of MaxSamples
    """

    range_module1 = None
    range_module2 = None
    range_module3 = None
    range_module4 = None
    """
        Module 1 Run
    """
    i = 0
    test1 = True
    while (test1) and (i<MaxSamples):
        range_module1 = DWM1000_module1.loop()
        i = i + 1
        if range_module1 != None:

            print('Range module 1: %.3f' %(range_module1))
            test1 = False


    """
        Module 2 Run
    """

    i = 0
    test2 = True
    while (test2) and (i<MaxSamples):
        range_module2 = DWM1000_module2.loop()
        i = i + 1
        if range_module2 != None:
            print('Range module 2: %.3f' %(range_module2))
            test2 = False



    """
        Module 3 Run
    """

    i = 0
    test3 = True
    while (test3) and (i<MaxSamples):
        range_module3 = DWM1000_module3.loop()
        i = i + 1
        if range_module3 != None:
            print('Range module 3: %.3f' %(range_module3))
            test3 = False

    """
        Module 4 Run
    """

    i = 0
    test4 = True
    while (test4) and (i<MaxSamples):
       range_module4 = DWM1000_module4.loop()
       i = i + 1
       if range_module4 != None:
            test4 = False
            print('Range module 4: %.3f' %(range_module4))

    """
        End of module loops
    """

    # print('One loop')

    # if range_module1 != None:
    #     print('Range module 1: %.3f' %(range_module1))
    # if range_module2 != None:
    #     print('Range module 2: %.3f' %(range_module2))
    # if range_module3 != None:
    #     print('Range module 3: %.3f' %(range_module3))
    # if range_module4 != None:
    #     print('Range module 4: %.3f' %(range_module4))


def main():
    try:
        while 1:
            das_loop()
    except KeyboardInterrupt:
        print('    Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
