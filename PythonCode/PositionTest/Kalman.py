

import numpy as np
import math

global wheeldiameter
global wheelbase
global encoderpulse
#global uwb_module_distance



def KalmanPosition(z, position_old, P_old, DeltaTime, Q, R, ModulePositions):
    F = np.matrix([[1, 0], [0, 1]])
    B = np.matrix([[0], [0]]) #np.transpose([0,0])

    x_hat_priori = F * position_old + B#*controlsignal
    P_priori_part = np.matmul(F, P_old)
    P_priori = np.matmul(P_priori_part, np.transpose(F)) + Q

    P_priori = F*P_old*np.transpose(F) + Q

    x_hat_priori_x = x_hat_priori[0,0]
    x_hat_priori_y = x_hat_priori[1,0]

    #print(x_hat_priori_x)
    # Module positions
    #Front left
    module_1x =  ModulePositions[0,0]
    module_1y =  ModulePositions[0,1]
    #Front right
    module_2x =    ModulePositions[1,0]
    module_2y =    ModulePositions[1,1]
    #Back left
    module_3x =    ModulePositions [2,0]
    module_3y =    ModulePositions[2,1]
    #Back right
    module_4x =    ModulePositions[3,0]
    module_4y =    ModulePositions[3,1]

    h_priori =  np.zeros((4, 1))
    #h_priori = np.matrix([[0], [0], [0], [0]])
    #print(h_priori.shape)
    #print(module_1x)
    #a = math.pow(x_hat_priori_x - module_1x,2)
    #print(a)
    #b = math.pow(x_hat_priori_y - module_1y,2)

    #print(a, b)

    #test = math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))
    #h_priori[0,0] = 1
    h_priori[0,0] = math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))
    h_priori[1,0] = math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))
    h_priori[2,0] = math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))
    h_priori[3,0] = math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))

    #h_priori = np.array([[math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))],
    #            [math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))],
    #            [math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))],
    #            [math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))]])


    H = np.zeros((4,2))

    H[0,0] = (x_hat_priori_x - module_1x)/math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))
    H[1,0] = (x_hat_priori_x - module_2x)/math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))
    H[2,0] = (x_hat_priori_x - module_3x)/math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))
    H[3,0] = (x_hat_priori_x - module_4x)/math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))


    H[0,1] = (x_hat_priori_y - module_1y)/math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))
    H[1,1] = (x_hat_priori_y - module_2y)/math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))
    H[2,1] = (x_hat_priori_y - module_3y)/math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))
    H[3,1] = (x_hat_priori_y - module_4y)/math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))


    #H = np.array([[(x_hat_priori_x - module_1x)/math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2)), (x_hat_priori_y - module_1y)/math.sqrt(math.pow(x_hat_priori_x - module_1x,2) + math.pow(x_hat_priori_y - module_1y,2))]
    #     [(x_hat_priori_x - module_2x)/math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2)), (x_hat_priori_y - module_2y)/math.sqrt(math.pow(x_hat_priori_x - module_2x,2) + math.pow(x_hat_priori_y - module_2y,2))]
    #     [(x_hat_priori_x - module_3x)/math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2)), (x_hat_priori_y - module_3y)/math.sqrt(math.pow(x_hat_priori_x - module_3x,2) + math.pow(x_hat_priori_y - module_3y,2))]
    #     [(x_hat_priori_x - module_4x)/math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2)), (x_hat_priori_y - module_4y)/math.sqrt(math.pow(x_hat_priori_x - module_4x,2) + math.pow(x_hat_priori_y - module_4y,2))]])
    #print(z.shape)
    #print(h_priori.shape)
    h_priori = np.mat(h_priori)
    H = np.mat(H)
    y = z - h_priori
    #print(y.shape)

    #S_part = np.matmul(H, P_priori)
    #H_transpose = np.transpose(H)
    #S = np.matmul(S_part, H_transpose) + R

    S = H*P_priori*H.T + R

    #K_part = np.matmul(P_priori, H_transpose)
    #S_inverse = np.linalg.inv(S)
    #Kalman_gain = np.matmul(K_part, S_inverse)

    Kalman_gain = P_priori * H.T * S.I
    #print(Kalman_gain.shape)
   #Outputs
    MeasuredPrediction = np.matmul(Kalman_gain, y)
    #print(x_hat_priori.shape)
    #print(MeasuredPrediction.shape)
    uwb_position = x_hat_priori + Kalman_gain * y# + MeasuredPrediction
    P_part = np.eye(2) - np.matmul(Kalman_gain, H)
    P_part = np.eye(2) - Kalman_gain*H
    #P_new = np.matmul(P_part, P_priori)
    P_new = P_part *P_priori
    return uwb_position, P_new
