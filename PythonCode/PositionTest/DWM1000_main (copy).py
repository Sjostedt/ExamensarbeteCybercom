#Dis tha main

#import time
#import DW1000
import monotonic
import DW1000Constants as C
import rospy
from DW1000RangingAnchor_class import *
import numpy as np
import math

from Kalman import *


def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """
    return int(round(monotonic.monotonic() * C.MILLISECONDS))
#Define RPi output pins for the UWB modules
CS1 = 16
CS2 = 20
CS3 = 21
CS4 = 12
CS5 = 23
IRQ1 = 19
IRQ2 = 26
IRQ3 = 13
IRQ4 = 6
IRQ5 = 22
"""     Antenna delays for each module      """
M1delay = 16311
M2delay = 16332
M3delay = 16317
M4delay = 16333


MaxSamples = 40000
moduleHz = 0.5 #maximum allowed samplingrate per module
moduleTs = 1/moduleHz
StartupTime = 1

"""    Define variables     """
#Rover specific

global wheeldiameter
global wheelbase
global encoderpulse

global UwbKalmanPos
global Q
global R
global Pcov_new
global ModulePositions


#global uwb_module_distance
wheeldiameter= 0.24 # Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946 #Distance traveled per tic
wheelbase = 0.4645    # Wheel base [m]
encoderpulse =  349 # %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / math.pi
uwb_module_distance = 0.3

#Kalman specific
Pcov_new = 0.1 * np.eye(2)
uwb_total_offset = 0.1
process_variance = 0.05
uwb_variance = math.sqrt((uwb_total_offset*0.75));
R = math.pow(uwb_variance,2) * np.eye(4)
Q = process_variance * np.eye(2)
UwbKalmanPos = np.matrix([[0], [0]])
#print(UwbKalmanPos.shape)





# Module positions
#Front left
module_1x =  0.235
module_1y =  0.275
#Front right
module_2x =    0.235
module_2y =    -0.275
#Back left
module_3x =    -0.235
module_3y =    0.275
#Back right
module_4x =    -0.235
module_4y =    -0.275

ModulePositions = np.array([[module_1x, module_1y], [module_2x, module_2y], [module_3x, module_3y], [module_4x, module_4y]])


#Random
TimeOld = millis()

# Create instance of all modules
time.sleep(StartupTime)
DWM1000_module1 = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1, M1delay)
print('\n')
time.sleep(StartupTime)
DWM1000_module2 = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2, M2delay)
print('\n')
time.sleep(StartupTime)
DWM1000_module3 = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3, M3delay)
time.sleep(StartupTime)
print('\n')
DWM1000_module4 = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4, M4delay)
time.sleep(StartupTime)
print('\n')
time.sleep(StartupTime)

#time.sleep(0.006)
#Add 5th module if applied
#DWM1000_module5 = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)


debug = False

def das_loop():

    global UwbKalmanPos
    global Q
    global R
    global Pcov_new
    global ModulePositions
    global debug

    #print(ModulePositions)
    #print(np.shape(ModulePositions))

    DeltaTime = millis() - TimeOld
    #Loop for calculating the range


    """     Loop for all modules. Returns range to tag or tries of MaxSamples       """

    """     Module 1 Run    """
    i = 0
    test1 = True
    k = 0
    while (test1) and (i<MaxSamples):
        range_module1 = DWM1000_module1.loop()
        i = i + 1
        if range_module1 != None:
            if debug:
                print('Range module 1: %.3f' %(range_module1))
            if range_module1 > 100:
                range_module1 = 0.3 # Fix later
            if k == 0:
                range_module1_1 = range_module1
            elif k == 1:
                range_module1_2 = range_module1
            k = k+1
            if k >1:
                range_module1 = (range_module1_1 + range_module1_2)/2
                test1 = False


    """         Module 2 Run        """

    i = 0
    test2 = True
    k = 0
    while (test2) and (i<MaxSamples):
        range_module2 = DWM1000_module2.loop()
        i = i + 1
        if range_module2 != None:
            if debug:
                print('Range module 2: %.3f' %(range_module2))
            if range_module2 > 100:
                range_module2 = 0.3 # Fix later
            if k == 0:
                range_module2_1 = range_module2
            elif k == 1:
                range_module2_2 = range_module2
            k = k+1
            if k >1:
                range_module2 = (range_module2_1 + range_module2_2)/2
                test2 = False



    """         Module 3 Run        """

    i = 0
    test3 = True
    k = 0
    while (test3) and (i<MaxSamples):
        range_module3 = DWM1000_module3.loop()
        i = i + 1
        if range_module3 != None:
            if debug:
                print('Range module 3: %.3f' %(range_module3))
            if range_module3 > 100:
                range_module3 = 0.3 # Fix later
            if k == 0:
                range_module3_1 = range_module3
            elif k == 1:
                range_module3_2 = range_module3
            k = k+1
            if k >1:
                range_module3 = (range_module3_1 + range_module3_2)/2
                test3 = False

    """         Module 4 round      """

    i = 0
    test4 = True
    k = 0
    while (test4) and (i<MaxSamples):
        range_module4 = DWM1000_module4.loop()
        i = i + 1
        if range_module4 != None:
            if debug:
                print('Range module 4: %.3f' %(range_module4))
            if range_module4 > 100:
                range_module4 = 0.3 # Fix later
            if k == 0:
                range_module4_1 = range_module4
            elif k == 1:
                range_module4_2 = range_module4
            k = k+1
            if k >1:
                range_module4 = (range_module4_1 + range_module4_2)/2
                test4 = False
    """     End of module loops     """

    """     Run position filtering and outputs      """


    z = np.matrix([[range_module1], [range_module2], [range_module3], [range_module4]])

    if np.any(z) == True:

        UwbKalmanPos, Pcov_new = KalmanPosition(z, UwbKalmanPos, Pcov_new, DeltaTime, Q, R, ModulePositions)

        print('x position is: %.2f' %(UwbKalmanPos[0,0]))
        print('y position is: %.2f' %(UwbKalmanPos[1,0]))
        print('----------------------------')
        print(z)


def main():
    try:
        while 1:
            das_loop()
    except KeyboardInterrupt:
        print('    Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
