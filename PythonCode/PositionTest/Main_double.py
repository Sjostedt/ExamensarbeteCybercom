#Dis tha main

#import time
#import DW1000
import monotonic
import DW1000Constants as C
import rospy
from DW1000RangingAnchor_class import *
import numpy as np
import math

from Kalman import *

from position_publisher.msg import position
from std_msgs.msg import String


global wheeldiameter
global wheelbase
global encoderpulse

global UwbKalmanPos
global Q
global R
global Pcov_new
global ModulePositions
global MaxSamples

def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """
    return int(round(monotonic.monotonic() * C.MILLISECONDS))
#Define RPi output pins for the UWB modules
CS1 = 16
CS2 = 20
CS3 = 21
CS4 = 12
CS5 = 23
IRQ1 = 19
IRQ2 = 26
IRQ3 = 13
IRQ4 = 6
IRQ5 = 22
"""     Antenna delays for each module      """
M1delay = 16325
M2delay = 16346
M3delay = 16332
M4delay = 16347


MaxSamples = 70000
moduleHz = 0.5 #maximum allowed samplingrate per module
moduleTs = 1/moduleHz
StartupTime = 1

"""    Define variables     """
#Rover specific



#global uwb_module_distance
wheeldiameter= 0.24 # Wheel diameter [m]
WHEEL_METER_PER_TICK = 0.002204790830946 #Distance traveled per tic
wheelbase = 0.4645    # Wheel base [m]
encoderpulse =  349 # %128*8; %Number of pulses per revolution
wheeldiameter = encoderpulse * WHEEL_METER_PER_TICK / math.pi
uwb_module_distance = 0.3

#Kalman specific

#uwb_total_offset = 0.15
process_variance = 0.0001
#uwb_variance = math.sqrt((uwb_total_offset*0.75));
uwb_variance = 0.00076
R = uwb_variance * np.eye(4)
Q = process_variance * np.eye(2)
UwbKalmanPos = np.matrix([[8], [0]])
#print(UwbKalmanPos.shape)





# Module positions
ModuleX = 0.555/2
ModuleY = 0.47/2
#Front left
module_1x =  ModuleX
module_1y =  ModuleY
#Front right
module_2x =    ModuleX
module_2y =    -ModuleY
#Back left
module_3x =    -ModuleX
module_3y =    ModuleY
#Back right
module_4x =    -ModuleX
module_4y =    -ModuleY

ModulePositions = np.array([[module_1x, module_1y], [module_2x, module_2y], [module_3x, module_3y], [module_4x, module_4y]])


#Random
TimeOld = millis()
def ModuleInit():
# Create instance of all modules
    time.sleep(StartupTime)
    DWM1000_module1 = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1, M1delay)
    print('\n')
    time.sleep(StartupTime)
    DWM1000_module2 = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2, M2delay)
    print('\n')
    time.sleep(StartupTime)
    DWM1000_module3 = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3, M3delay)
    time.sleep(StartupTime)
    print('\n')
    DWM1000_module4 = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4, M4delay)
    time.sleep(StartupTime)
    print('\n')
    time.sleep(StartupTime)

    return DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4
#time.sleep(0.006)
#Add 5th module if applied
#DWM1000_module5 = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)


debug = False

def das_loop(p, DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4, Pcov_new, UwbKalmanPos):

    #global UwbKalmanPos
    global Q
    global R
    #global Pcov_new
    global ModulePositions
    global debug
    global MaxSamples

    pub = rospy.Publisher('uwb_position', position, queue_size=1)
    rospy.init_node('DWM1000_main', anonymous=True)
    rate = rospy.Rate(40) # 40hz
    msg = position()
    if debug:
        print('start loop')

    totaltime_stamp1 = monotonic.monotonic()



    #print(ModulePositions)
    #print(np.shape(ModulePositions))

    DeltaTime = millis() - TimeOld
    #Loop for calculating the range


    """     Loop for all modules. Returns range to tag or tries of MaxSamples       """

    """     Module 1 Run    """
    startsample = monotonic.monotonic()
    i = 0
    test1 = True
    k = 0
    while (test1) and (i<MaxSamples):
        range_module1 = DWM1000_module1.loop()
        i = i + 1
        if debug:
            if i == 1:
                print('Module 1')
            print(i)
        if range_module1 != None:
            if debug:
                print('Range module 1: %.3f' %(range_module1))
            if range_module1 > 100:
                range_module1 = 0.3 # Fix later
            if k == 0:
                range_module1_1 = range_module1
            elif k == 1:
                range_module1_2 = range_module1
            k = k+1
            if k >1:
                range_module1 = (range_module1_1 + range_module1_2)/2

                test1 = False


    """         Module 2 Run        """

    i = 0
    test2 = True
    k = 0
    while (test2) and (i<MaxSamples):
        range_module2 = DWM1000_module2.loop()
        i = i + 1
        if debug:
            if i == 1:
                print('Module 2')
            print(i)
        if range_module2 != None:
            if debug:
                print('Range module 2: %.3f' %(range_module2))
            if range_module2 > 100:
                range_module2 = 0.3 # Fix later
            if k == 0:
                range_module2_1 = range_module2
            elif k == 1:
                range_module2_2 = range_module2
            k = k+1
            if k >1:
                range_module2 = (range_module2_1 + range_module2_2)/2

                test2 = False



    """         Module 3 Run        """

    i = 0
    test3 = True
    k = 0
    while (test3) and (i<MaxSamples):
        range_module3 = DWM1000_module3.loop()
        i = i + 1
        if debug:
            if i == 1:
                print('Module 3')
            print(i)
        if range_module3 != None:
            if debug:
                print('Range module 3: %.3f' %(range_module3))
            if range_module3 > 100:
                range_module3 = 0.3 # Fix later
            if k == 0:
                range_module3_1 = range_module3
            elif k == 1:
                range_module3_2 = range_module3
            k = k+1
            if k >1:
                range_module3 = (range_module3_1 + range_module3_2)/2

                test3 = False

    """         Module 4 round      """

    i = 0
    test4 = True
    k = 0
    while (test4) and (i<MaxSamples):
        range_module4 = DWM1000_module4.loop()
        i = i + 1
        if debug:
            if i == 1:
                print('Module 4')
            print(i)
        if range_module4 != None:
            if debug:
                print('Range module 4: %.3f' %(range_module4))
            if range_module4 > 100:
                range_module4 = 0.3 # Fix later
            if k == 0:
                range_module4_1 = range_module4
            elif k == 1:
                range_module4_2 = range_module4
            k = k+1
            if k >1:
                range_module4 = (range_module4_1 + range_module4_2)/2

                test4 = False
    """     End of module loops     """

    """     Run position filtering and outputs      """
    enduwbsample = monotonic.monotonic()
    uwbsampletime = enduwbsample-startsample

    #print(startsample)
    #print(enduwbsample)
    #print(uwbsampletime)

    z = np.matrix([[range_module1], [range_module2], [range_module3], [range_module4]])

    if np.any(z) == True:
        startkalman = monotonic.monotonic()
        UwbKalmanPos, Pcov_new = KalmanPosition(z, UwbKalmanPos, Pcov_new, DeltaTime, Q, R, ModulePositions)
        kalmansampletime = (monotonic.monotonic() - startkalman)*1000


        print('x position is: %.2f' %(UwbKalmanPos[0,0]))
        print('y position is: %.2f' %(UwbKalmanPos[1,0]))
        print('----------------------------')
        print(z)
        print('----------------------------')
        print('Kalman loop runs for %.3f milliseconds' %(kalmansampletime))
        msg.X = UwbKalmanPos[0,0]
        msg.Y = UwbKalmanPos[1,0]
        pub.publish(msg)
        rate.sleep()
        p = 0


    else:
        p = p +1
        print('NO POSITION')
        # if p == 3:
        #     DWM1000_module1.ExitModule()
        #     DWM1000_module2.ExitModule()
        #     DWM1000_module3.ExitModule()
        #     DWM1000_module4.ExitModule()
        #     DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4 = ModuleInit()
        #     p = 0

    TotalSampleTime = monotonic.monotonic() - totaltime_stamp1
    TotalHz = 1/TotalSampleTime
    print('Sample time of modules is %.4f seconds' %(uwbsampletime))
    print('Frequency of position loop: %.3f' %(TotalHz))
    return p, DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4, Pcov_new, UwbKalmanPos



def main():
    try:
        Pcov_new = 0.1 * np.eye(2)
        UwbKalmanPos = np.matrix([[0], [0]])
        p = 0
        DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4 = ModuleInit()
        while 1:

            p, DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4, Pcov_new, UwbKalmanPos = das_loop(p, DWM1000_module1, DWM1000_module2, DWM1000_module3, DWM1000_module4, Pcov_new, UwbKalmanPos)
    except KeyboardInterrupt:
        print('    Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
