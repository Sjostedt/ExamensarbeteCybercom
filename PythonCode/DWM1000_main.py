#Dis tha main

import DW1000
import monotonic
import DW1000Constants as C
import rospy
#import time

from DW1000RangingAnchor_class import *

#Define RPi output pins for the UWB modules
CS1 = 16
CS2 = 20
CS3 = 21
CS4 = 12
CS5 = 23
IRQ1 = 19
IRQ2 = 26
IRQ3 = 13
IRQ4 = 6
IRQ5 = 22

moduleHz = 0.5 #maximum allowed samplingrate per module
moduleTs = 1/moduleHz
#DWM1000_module1 = DWM1000_ranging("module1", "82:17:5B:D5:A9:9A:E2:1A", CS1, IRQ1)
#DWM1000_module2 = DWM1000_ranging("module2", "82:17:5B:D5:A9:9A:E2:2A", CS2, IRQ2)
#DWM1000_module3 = DWM1000_ranging("module3", "82:17:5B:D5:A9:9A:E2:3A", CS3, IRQ3, 1)
#time.sleep(1)
DWM1000_module4 = DWM1000_ranging("module4", "82:17:5B:D5:A9:9A:E2:4A", CS4, IRQ4)

#Add 5th module if applied
#DWM1000_module5 = DWM1000_ranging("module5", "82:17:5B:D5:A9:9A:E2:5A", CS5, IRQ5)



def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """
    return int(round(monotonic.monotonic() * C.MILLISECONDS))


def das_loop():
    #Loop for calculating the range
    #rangemodule1_old = None
    #DWM1000_module3.printa()
    #DWM1000_module4.printa()
    range_module1 = None
    range_module2 = None
    range_module3 = None
    range_module4 = None
    # test = True
    #
    # StartTime = millis()*1000
    # while (millis()*1000 - StartTime < moduleTs) and (test):
    #     range_module3 = DWM1000_module3.loop()
    #     if range_module3 != None:
    #         #rangemodule1_old = range_module1
    #         # print(range_module3)
    #         test = False
    test1 = True
    StartLoop = monotonic.monotonic()
    StartTime = millis() * 1000
    while (test1):
       range_module4 = DWM1000_module4.loop()
       if range_module4 != None:
            #rangemodule1_old = range_module1
            test1 = False
    endloop = monotonic.monotonic() - StartLoop
    loopHZ = 1/endloop
    print(loopHZ)
    #range_module2 = DWM1000_module.loop()
    #range_module3 = DWM1000_module.loop()
    #range_module4 = DWM1000_module.loop()
    if range_module3 != None:
        print('Range module 3: %.3f' %(range_module3))
    if range_module4 != None:
        print('Range module 4: %.3f' %(range_module4))

    #print('Range module 4: %.3f' %(range_module4))
    #print('Range module 1: %.3f' %(range_module1))
    #print(range_module1)




    #except KeyboardInterrupt:
        #DW1000.close()


def main():
    try:
        while 1:
            das_loop()
    except KeyboardInterrupt:
        print('    Interrupted by user')


if __name__ == '__main__':
#Run the main loop.
    main()
